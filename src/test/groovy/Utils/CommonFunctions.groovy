package Utils

import GroovyAutomation.FunctionalLibrary
import Pages.AFHSHomePage
import Pages.BillingPage
import Pages.CartPage
import Pages.Header
import Pages.LoginPage
import Pages.OrderDetails
import Pages.PaymentPage
import Pages.ProductDetailsPage
import Pages.SearchResultsPage
import Pages.ShippingPage
import Pages.StoreLocatorPage
import Pages.ThankYouPage
import Pages.WelcomePage
import geb.Browser
import geb.error.RequiredPageContentNotPresent
import geb.spock.GebSpec
import org.openqa.selenium.Keys


final class CommonFunctions extends GebSpec {

    public static Browser currentBrowser = null;

   /* def static void closeOverlay() {
        FunctionalLibrary.Click(currentBrowser.at(AFHSHomePage).noThankYouLink)
//        FunctionalLibrary.WaitForPageLoad()
    }*/

    def static void searchForProduct(String productName, IsBoolean = true) {
        if(IsBoolean == true) {
            FunctionalLibrary.SetText(currentBrowser.at(Header).searchInputField, productName)
            Thread.sleep(10000)
            FunctionalLibrary.Click(currentBrowser.at(Header).searchInputButton)
            FunctionalLibrary.WaitForPageLoad()
        } else {
            FunctionalLibrary.SetText(currentBrowser.at(Header).SubCategorysearchInputField, productName)
            FunctionalLibrary.Click(currentBrowser.at(Header).SubCategorysearchInputButton)
            FunctionalLibrary.WaitForPageLoad()
        }
    }


//    static int index = 0;
//   def static void selectProductFromSearchResultsPage() {
//        //// FunctionalLibrary.WaitForPageLoad()
//        //if (currentBrowser.at(SearchResultsPage).products.getAt(index).find('img').first().getAttribute("class") != "promo-card") {
//        if (currentBrowser.at(SearchResultsPage).results.children("li").getAt(index).find('img').first().getAttribute("class") != "promo-card") {
//            //  FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).products.getAt(index).find('img').first())
//            FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).results.children("li").getAt(index).find('img').first())
//            FunctionalLibrary.WaitForPageLoad()
//            index = 0;
//        } else {
//            index = index + 1
//            selectProductFromSearchResultsPage()
//        }
//    }

//    def static firstProduct(){
//
//        currentBrowser.at(SearchResultsPage).results.children("li").find("div[class ~= 'productImage']").find('img').first().parent()
//    }
//
//    def static void selectProductFromSearchResultsPage()
//    {
//        FunctionalLibrary.Click(firstProduct().find('img'))
//        FunctionalLibrary.WaitForPageLoad(60)
//
//    }

//    static int index = 0;
//    static def chosenProductIndex = null
//    def static void clickCheckPriceOfProduct() {
//       if (currentBrowser.at(SearchResultsPage).results.children("li").getAt(index).find('img').first().getAttribute("class") != "promo-card") {
//            chosenProductIndex = index
//            if (FunctionalLibrary.IsElementExist("a.checkPriceModalLink", currentBrowser.at(SearchResultsPage).results.children("li").getAt(index))) {
//                FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).results.children("li").getAt(index).find("a.checkPriceModalLink").first(),15)
//                index = 0;
//            } else {
//                index = index + 1
//                clickCheckPriceOfProduct()
//            }
//        } else {
//            index = index + 1
//            clickCheckPriceOfProduct()
//        }
//    }

//    static int i = 0
//    def static void clickQuickViewOfProduct() {
//        ////FunctionalLibrary.WaitForPageLoad()
//        def productContainer = currentBrowser.at(SearchResultsPage).products.getAt(i)
//        if (productContainer.find('img').first().getAttribute("class") != "promo-card") {
//            currentBrowser.js.exec(productContainer.find('img').first().firstElement(), "jQuery(arguments[0]).mouseover();")
//            def quickView = productContainer.find("a", text: "Quick View").findAll()
////            currentBrowser.js.exec(quickView[0].firstElement(), "jQuery(arguments[0]).click();")
//            FunctionalLibrary.Click(quickView[0],15)
//            i = 0;
//        } else {
//            i = i + 1
//            clickQuickViewOfProduct()
//        }
//    }


//       def static void clickQuickViewOfProduct()
//        {
//                 currentBrowser.js.exec(firstProduct().find('img').first().firstElement(), "jQuery(arguments[0]).mouseover();")
//                 FunctionalLibrary.Click(firstProduct().parent().find("a.quickView").findAll()[0],5)
//
//    }


    def static void enableAddToCart(String zipCode) {
        FunctionalLibrary.WaitForPageLoad(40)
        def listChildren = [];
        currentBrowser.at(ProductDetailsPage).buttonsContainer.children().each { x -> if (x.isDisplayed() == true) listChildren << x }

        String innerText = listChildren.get(0).text()

        if (!innerText.toLowerCase().contains("find store")) {
            if (innerText.toLowerCase() == "check price") {
                FunctionalLibrary.Click(listChildren.get(0))
                FunctionalLibrary.SetText(currentBrowser.at(ProductDetailsPage).zipCodeText, zipCode)
                FunctionalLibrary.Click(currentBrowser.at(ProductDetailsPage).zipCodeSearchButton)
                Thread.sleep(20000)
                FunctionalLibrary.WaitForPageLoad()
            }
        }else{
            throw new Exception("Given product is not available in ATP")
        }
    }

    def static void  addProductToCart(String zipCode = "",String quantity="1",boolean IsQuickView = false, boolean IsSearchResults = true) {

        def listChildren = [];
        if(IsSearchResults == true) {
            currentBrowser.at(ProductDetailsPage).buttonsContainer.find("a").each { x -> if (x.isDisplayed() == true) listChildren << x }
        } else {
            currentBrowser.at(ProductDetailsPage).buttonsContainerMouseOverQuickView.find("button").each { x -> if (x.isDisplayed() == true) listChildren << x }
        }

        String innerText = listChildren.get(0).text()

        if (!innerText.toLowerCase().contains("find store")) {
            if(innerText.toLowerCase() == "check price") {
                FunctionalLibrary.Click(listChildren.get(0))
                FunctionalLibrary.SetText(currentBrowser.at(ProductDetailsPage).zipCodeText, zipCode)
                FunctionalLibrary.Click(currentBrowser.at(ProductDetailsPage).zipCodeSearchButton)
                Thread.sleep(40000)
//                FunctionalLibrary.WaitForPageLoad()
                FunctionalLibrary.SetText(currentBrowser.at(ProductDetailsPage).productQuantity, quantity)
                FunctionalLibrary.Focus(currentBrowser.at(ProductDetailsPage).addToCartButton)
                FunctionalLibrary.Click(currentBrowser.at(ProductDetailsPage).addToCartButton,150,true)
                Thread.sleep(40000)
//                FunctionalLibrary.WaitForPageLoad()
            }
            else if (innerText.toLowerCase() == "add to cart") {
                if(IsSearchResults == true) {
                    FunctionalLibrary.SetText(currentBrowser.at(ProductDetailsPage).productQuantity, quantity)
                } else {
                    FunctionalLibrary.SetText(currentBrowser.at(ProductDetailsPage).productQuantityatQuickViewLightbox, quantity)
                }

                if(IsQuickView == false) {
                    FunctionalLibrary.Focus(currentBrowser.at(ProductDetailsPage).addToCartButton)
                    FunctionalLibrary.Click(currentBrowser.at(ProductDetailsPage).addToCartButton,150,true)
                    Thread.sleep(40000)
//                    FunctionalLibrary.WaitForPageLoad()
                }
                else if (IsSearchResults == true) {
                    FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).addToCartButtonQuickView)
                } else {
                    FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).addToCartbtnatQuickViewLightbox)
                }
                Thread.sleep(40000)
//               FunctionalLibrary.WaitForPageLoad(30)
                if(!zipCode.isEmpty()) {
                    FunctionalLibrary.SetText(currentBrowser.at(ProductDetailsPage).zipCodeText, zipCode)
                    FunctionalLibrary.Click(currentBrowser.at(ProductDetailsPage).zipCodeSearchButton)
                    Thread.sleep(40000)
                }
            }

        } else {
            throw new Exception("Given product is not available in ATP")
        }
    }

//    def static void clickCheckPriceFromSearchResults(String zipCode) {
//        clickCheckPriceOfProduct()
//        currentBrowser.at(SearchResultsPage).zipCodeText.value(zipCode) << Keys.ENTER
//        FunctionalLibrary.WaitForPageLoad()
//        currentBrowser.js.exec(currentBrowser.at(SearchResultsPage).results.children("li").getAt(chosenProductIndex).find('img').first().firstElement(), "jQuery(arguments[0]).mouseover();")
//        def quickView = currentBrowser.at(SearchResultsPage).results.children("li").getAt(chosenProductIndex).find("a", text: "Quick View").findAll()
//        FunctionalLibrary.Click(quickView[0], 40)
//    }


//    def static void userLogin(String username, String password) {
//        FunctionalLibrary.SetText(currentBrowser.at(LoginPage).username, username)
//        FunctionalLibrary.SetText(currentBrowser.at(LoginPage).password, password)
//        FunctionalLibrary.Focus(currentBrowser.at(LoginPage).loginButton)
////        currentBrowser.at(LoginPage).loginButton.click()
//        FunctionalLibrary.Click(currentBrowser.at(LoginPage).loginButton,150,true)
////        FunctionalLibrary.WaitForPageLoad()
//    }

//    def static void newUserAccountCreation(String FirstName, String LastName, String Email, String Password) {
//
//        FunctionalLibrary.SetText(currentBrowser.at(LoginPage).firstName, FirstName)
//        FunctionalLibrary.SetText(currentBrowser.at(LoginPage).lastName, LastName)
//        FunctionalLibrary.SetText(currentBrowser.at(LoginPage).email, Email)
//        FunctionalLibrary.SetText(currentBrowser.at(LoginPage).confirmEmail, Email)
//        FunctionalLibrary.SetText(currentBrowser.at(LoginPage).createPassword, Password)
//        FunctionalLibrary.SetText(currentBrowser.at(LoginPage).confirmPassword, Password)
//        currentBrowser.at(LoginPage).ageConfirmCheckBox.value(true)
//        FunctionalLibrary.Click(currentBrowser.at(LoginPage).createAccountButton,150,true)
////        currentBrowser.at(LoginPage).createAccountButton.click()
////        FunctionalLibrary.WaitForPageLoad()
//    }

    def static void myAccountOptionSelection(String myAccountOption) {

        currentBrowser.js.exec(currentBrowser.at(AFHSHomePage).myAccount.firstElement(), "jQuery(arguments[0]).mouseover();")
        FunctionalLibrary.Click(currentBrowser.at(Header).dropDownParent.find("a", text: myAccountOption))
        FunctionalLibrary.WaitForPageLoad()
    }

//    def static void myAccountSideTabOptionSelection(myAccountOption) {
////        Thread.sleep(1500)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).container.find("a", text: myAccountOption).findAll {
//            it.displayed
//        })
////        FunctionalLibrary.WaitForPageLoad()
//    }


    def static void clickLastWishListAtSideTab() {
        if (currentBrowser.at(WelcomePage).wishListingContainerSideTab.children().size() > 1) {
            FunctionalLibrary.Click(currentBrowser.at(WelcomePage).wishListLast,20)
        } else {
            throw new Exception("No wishlists were found for logged in user")
        }
    }

    def static void chooseSubCategory(String categoryName, String subCategoryName,String subCategoryGroupName="") {

        def categoryLinkCollection = currentBrowser.at(Header).navigationParent.find("li[class ~= 'afhs-mega-dropdown']")

        def categoryLink = null;

        for (int i = 0; i < categoryLinkCollection.size(); i++) {
            if (categoryLinkCollection.getAt(i).find("a.show-js").text() == categoryName) {
                //currentBrowser.page.interact {moveToElement(categoryLinkCollection.getAt(i).find("a.show-js"))}
                currentBrowser.js.exec(categoryLinkCollection.getAt(i).find("a.show-js").findAll {
                    it.displayed
                }.firstElement(), "jQuery(arguments[0]).mouseover();")

                categoryLink = categoryLinkCollection.getAt(i)
                break
            }
        }
        def dropDownParent = categoryLink.find("ul", class: "dropdown")

//        def groupCollection = dropDownParent.find("ol", class: "afhs-mega-nav-group").find("li.afhs-mega-nav-parent")
//
//        for (int j = 0; j < groupCollection.size(); j++) {
//            if(groupCollection[j].text().contains(subCategoryGroupName)) {
//                FunctionalLibrary.Click(groupCollection.getAt(j).find("a", text: subCategoryName))
//                break
//            }
//        }

        def groupCollection = dropDownParent.find("ol", class: "afhs-mega-nav-group")

        def group = groupCollection.find(("li.afhs-mega-nav-parent"))

        for (int j = 0; j < groupCollection.size(); j++) {
            if (subCategoryGroupName == "") {
                FunctionalLibrary.Click(groupCollection.getAt(j).find("a", text: subCategoryName))
                FunctionalLibrary.WaitForPageLoad()
            } else {
                for (int k = 0; k < group.size(); k++) {
                    if (group[k].text().contains(subCategoryGroupName)) {
                        FunctionalLibrary.Click(group.getAt(k).find("a", text: subCategoryName),35)
                        break
                    }
                }
            }
            break
        }
        Thread.sleep(30000)
        FunctionalLibrary.WaitForPageLoad()
    }



    def static void updateQuantityInCart(String searchKeyword, String quantity) {

        def productNameAtRunTime = null;
        def currentRow = null;
        FunctionalLibrary.WaitForPageLoad(25)
        for (int i = 0; i < currentBrowser.at(CartPage).productRow.size(); i++) {
            def currentRowProductName = currentBrowser.at(CartPage).productRow.get(i).find("a.msax-ProductName")
            if (currentRowProductName != null) {
                productNameAtRunTime = currentBrowser.at(CartPage).productRow.get(i).find("a.msax-ProductName").findAll {
                    it.displayed
                }
                if (productNameAtRunTime.text().toLowerCase() == searchKeyword.toLowerCase())
                    currentRow = currentBrowser.at(CartPage).productRow.get(i)
                break;
            }
        }
        FunctionalLibrary.SetText(currentRow.find("input#txtbxQuantityIncrementalID"), quantity)
        FunctionalLibrary.Click(currentRow.find("button", text: "Update"),15)
//        FunctionalLibrary.WaitForPageLoad(20)
    }


//    def static void enterShippingDetails(String Firstname, String Lastname, String Addressline1, String City, String State, String Zipcode, String Phone, String Email) {
////        Thread.sleep(2000)
//        FunctionalLibrary.WaitForPageLoad()
//        FunctionalLibrary.SetText(currentBrowser.at(ShippingPage).firstName, Firstname)
//        FunctionalLibrary.SetText(currentBrowser.at(ShippingPage).lastName, Lastname)
//        FunctionalLibrary.SetText(currentBrowser.at(ShippingPage).addressLine1, Addressline1)
//        FunctionalLibrary.SetText(currentBrowser.at(ShippingPage).city, City)
//        currentBrowser.at(ShippingPage).state.click()
//        currentBrowser.at(ShippingPage).state.find("option", text: State).click()
//        currentBrowser.at(ShippingPage).state.click()
//        FunctionalLibrary.SetText(currentBrowser.at(ShippingPage).zipCode, Zipcode)
//        FunctionalLibrary.SetText(currentBrowser.at(ShippingPage).phone, Phone)
//        FunctionalLibrary.SetText(currentBrowser.at(ShippingPage).email, Email)
//        FunctionalLibrary.SetText(currentBrowser.at(ShippingPage).confirmEmail, Email)
////        currentBrowser.page.interact { moveToElement(currentBrowser.at(ShippingPage).continueButton) }
////        currentBrowser.js.exec(currentBrowser.at(ShippingPage).continueButton.firstElement(), "jQuery(arguments[0]).click();")
//        FunctionalLibrary.Click(currentBrowser.at(ShippingPage).continueButton)
//
//
//        if(! currentBrowser.at(ShippingPage).useThisAddressPopUp.isEmpty()) {
//            FunctionalLibrary.Click(currentBrowser.at(ShippingPage).useThisAddressOption)
//        }
////        FunctionalLibrary.WaitForPageLoad()
//    }


//    def static void enterBillingDetails(boolean useShippingAddressCheckboxVerfication = true,String BillingFirstName="", String BillingLastName="", String BillingAddressLine1="", String BillingCity="", String BillingState="", String BillingZipCode="", String BillingPhone="", String BillingEmail="") {
////        FunctionalLibrary.WaitForPageLoad()
//        if (useShippingAddressCheckboxVerfication == true) {
//            currentBrowser.at(BillingPage).useShippingAddressCheckbox.value(true)
//        } else {
//            FunctionalLibrary.SetText(currentBrowser.at(BillingPage).billingFirstName, BillingFirstName)
//            FunctionalLibrary.SetText(currentBrowser.at(BillingPage).billingLastName, BillingLastName)
//            FunctionalLibrary.SetText(currentBrowser.at(BillingPage).billingAddressLine1, BillingAddressLine1)
//            FunctionalLibrary.SetText(currentBrowser.at(BillingPage).billingCity, BillingCity)
//            currentBrowser.at(BillingPage).billingState.click()
//            currentBrowser.at(BillingPage).billingState.find("option", text: BillingState).click()
//            FunctionalLibrary.SetText(currentBrowser.at(BillingPage).billingZipCode, BillingZipCode)
//            FunctionalLibrary.SetText(currentBrowser.at(BillingPage).billingPhone, BillingPhone)
//            FunctionalLibrary.SetText(currentBrowser.at(BillingPage).billingEmail, BillingEmail)
//            FunctionalLibrary.SetText(currentBrowser.at(BillingPage).billingConfirmEmail, BillingEmail)
//
//        }
//        FunctionalLibrary.Click(currentBrowser.at(BillingPage).continueButtonBilling)
//        FunctionalLibrary.WaitForPageLoad()
//
////        FunctionalLibrary.WaitForPageLoad()
////        if (currentBrowser.driver.class.toString().endsWith("InternetExplorerDriver")) {
////            currentBrowser.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()")
////            FunctionalLibrary.WaitForPageLoad()
////        }
//
//    }

//    def static void enterCreditCardDetails(String firstName, String LastName, String Card, String SecurityCode, String expMonth, String expYear) {
////        FunctionalLibrary.WaitForPageLoad()
//        FunctionalLibrary.SetText(currentBrowser.at(PaymentPage).firstNameOnCard, firstName)
//        FunctionalLibrary.SetText(currentBrowser.at(PaymentPage).lastNameOnCard, LastName)
//        FunctionalLibrary.SetText(currentBrowser.at(PaymentPage).cardNumber, Card)
//        FunctionalLibrary.SetText(currentBrowser.at(PaymentPage).securityCode, SecurityCode)
//        currentBrowser.at(PaymentPage).expMonthOnCard.value(expMonth)
//        currentBrowser.at(PaymentPage).expYearOnCard.value(expYear)
//        FunctionalLibrary.Click(currentBrowser.at(PaymentPage).continueButtonPayment)
////        FunctionalLibrary.WaitForPageLoad()
//
//    }


//    def static void enterAshleyCardDetails(String ashleyCard, String financingOption) {
////        FunctionalLibrary.WaitForPageLoad()
//        FunctionalLibrary.Click(currentBrowser.at(PaymentPage).ashleyAdvantageCard)
//        FunctionalLibrary.SetText(currentBrowser.at(PaymentPage).ashleyCardNumber, ashleyCard)
//        FunctionalLibrary.Click(currentBrowser.at(PaymentPage).financingOptionContainer.children().find { x -> x.text().contains(financingOption) })
//        FunctionalLibrary.Click(currentBrowser.at(PaymentPage).viewFinanacingTerms)
////        FunctionalLibrary.WaitForPageLoad()
//        FunctionalLibrary.Click(currentBrowser.at(PaymentPage).termsOverlayAccept,25)
////        FunctionalLibrary.WaitForPageLoad()
//    }

//    def static void continueAsGuest() {
//        FunctionalLibrary.Click(currentBrowser.at(LoginPage).guestButton)
    //FunctionalLibrary.WaitForPageLoad()
//    }


//    def static void clickSubmitOrder() {
    // currentBrowser.page.interact { moveToElement(currentBrowser.at(ConfirmOrderPage).orderConfirmContinueButton) }
//      currentBrowser.js.exec(currentBrowser.at(ConfirmOrderPage).orderConfirmContinueButton.firstElement(), "jQuery(arguments[0]).click();")
//        FunctionalLibrary.Click(currentBrowser.at(ConfirmOrderPage).orderConfirmContinueButton)
//        FunctionalLibrary.WaitForPageLoad()
//    }

    def static void closeThankYouOverlay() {
        if (!currentBrowser.driver.class.toString().endsWith("FirefoxDriver")) {
//            FunctionalLibrary.WaitForPageLoad()
            FunctionalLibrary.Click(currentBrowser.at(ThankYouPage).orderConfirmCloseOverlay,5)
        }
    }


    def static void clearCart(String userName, String password) {
        currentBrowser.to(LoginPage)
        userLogin(userName, password)
        FunctionalLibrary.Click(currentBrowser.at(Header).cartIcon)
        clearProducts()
    }

    def static void clearProducts() {
        def rowCollection = currentBrowser.at(CartPage).cartTable.find("div.msax-ProductQuickLink")
        for (int i = rowCollection.size(); i > 0; i--) {
            FunctionalLibrary.Click(rowCollection[0].find("button", text: "Remove"),15)
            try {
                if (rowCollection.size() > 1) {
                    rowCollection = []
                    rowCollection = currentBrowser.at(CartPage).cartTable.find("div.msax-ProductQuickLink")
                }
            } catch (RequiredPageContentNotPresent ex) {
            }
        }
    }


//    def static void orderDetails(String orderNumber, String email, String zipCode) {
//        FunctionalLibrary.SetText(currentBrowser.at(OrderDetails).confirmationNumber, orderNumber)
//        FunctionalLibrary.SetText(currentBrowser.at(OrderDetails).emailAddress, email)
//        FunctionalLibrary.SetText(currentBrowser.at(OrderDetails).billingZipCode, zipCode)
//        FunctionalLibrary.Click(currentBrowser.at(OrderDetails).reviewOrder,150,true)
////        currentBrowser.at(OrderDetails).reviewOrder.click()
//    }

//    def static void findAStore(String Location, String Distance, String Country) {
//        FunctionalLibrary.SetText(currentBrowser.at(StoreLocatorPage).location,Location)
//        FunctionalLibrary.Click(currentBrowser.at(StoreLocatorPage).distance,150,true)
//        FunctionalLibrary.Click(currentBrowser.at(StoreLocatorPage).find("option", text: Distance),150,true)
//        FunctionalLibrary.Click(currentBrowser.at(StoreLocatorPage).country,150,true)
//        FunctionalLibrary.Click(currentBrowser.at(StoreLocatorPage).find("option", text: Country),150,true)
////        currentBrowser.at(StoreLocatorPage).distance.click()
////        currentBrowser.at(StoreLocatorPage).distance.find("option", text: Distance).click()
////        currentBrowser.at(StoreLocatorPage).country.click()
////        currentBrowser.at(StoreLocatorPage).country.find("option", text: Country).click()
//        FunctionalLibrary.Click(currentBrowser.at(StoreLocatorPage).findAStoreButton)
////        FunctionalLibrary.WaitForPageLoad()
//
//    }

//    def static void addAddressButtonClick() {
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).addAddress)
//        //  FunctionalLibrary.WaitForPageLoad()
//    }

//        def static void enterShippingDetailsAtAddressBook(String FirstName, String LastName, String Address, String City, String State, String Zipcode, String PhoneNumber, String Email) {
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).firstName, FirstName)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).lastName, LastName)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).address, Address)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).city, City)
//        currentBrowser.at(WelcomePage).state.click()
//        currentBrowser.at(WelcomePage).state.find("option", text: State).click()
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).zipCode, Zipcode)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).phoneNumber, PhoneNumber)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).email, Email)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).confirmEmail, Email)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).save)
//        //   FunctionalLibrary.WaitForPageLoad()
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).useThisAddress)
//
//    }

//    def static void addBillingAddressButtonClick() {
//        FunctionalLibrary.Click((currentBrowser.at(WelcomePage).addBillingAddress))
//        //  FunctionalLibrary.WaitForPageLoad()
//    }

//        def static void enterBillingDetailsAtAddressBook(String FirstName, String LastName, String Address, String City, String State, String Zipcode, String PhoneNumber, String Email) {
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).firstName, FirstName)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).lastName, LastName)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).address, Address)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).city, City)
//        currentBrowser.at(WelcomePage).state.click()
//        currentBrowser.at(WelcomePage).state.find("option", text: State).click()
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).zipCode, Zipcode)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).billingPhoneNumber, PhoneNumber)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).billingEmail, Email)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).billingConfirmEmail, Email)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).save)
//        //  FunctionalLibrary.WaitForPageLoad()
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).useThisAddress)
//    }

    def static void enterPromoCodeAtCart(String promocode) {
        currentBrowser.page.interact { moveToElement(currentBrowser.at(CartPage).applyPromoCodeButton) }
        currentBrowser.js.exec(currentBrowser.at(CartPage).applyPromoCodeButton.firstElement(), "jQuery(arguments[0]).click();")
//        FunctionalLibrary.SetText( currentBrowser.at(CartPage).enterPromoCodeLightBox,promocode)
        currentBrowser.at(CartPage).enterPromoCodeLightBox.value(promocode)
        FunctionalLibrary.Click(currentBrowser.at(CartPage).applyPromoCodeLightBox,20)
        FunctionalLibrary.Click(currentBrowser.at(CartPage).closeLightBox,5)
    }

//    def static void findAStoreButtonClick() {
//        FunctionalLibrary.Click(currentBrowser.at(StoreLocatorPage).findAStoreButton)
//    }
//
//    def static void getDirectionOnMap() {
//        FunctionalLibrary.Click(currentBrowser.at(StoreLocatorPage).getDirection)
//    }
//
//    def static void clickShopNameForShopDetails() {
//        FunctionalLibrary.Click(currentBrowser.at(StoreLocatorPage).shopName)
//    }


    def static void quickViewAddToWishList(String zipCode = "", String quantity) {
        def listChildren = [];
        Thread.sleep(20000)
        currentBrowser.at(ProductDetailsPage).buttonsContainerMouseOverQuickView.find("button").each { x -> if (x.isDisplayed() == true) listChildren << x }

        String innerText = listChildren.get(0).text()

        if (!innerText.toLowerCase().contains("add to cart")) {

            FunctionalLibrary.Click(listChildren.get(0),150,true)
//            FunctionalLibrary.SetText(currentBrowser.at(SearchResultsPage).zipCodeText, zipCode)
            FunctionalLibrary.SetText(currentBrowser.at(SearchResultsPage).quickViewZipCodeText, zipCode)
            FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).zipCodeSearchButton,20)
        }
        if(innerText.toLowerCase() == "add to cart") {
//            FunctionalLibrary.SetText(currentBrowser.at(SearchResultsPage).productQuantityatQuickViewLightboxMouseOver, quantity)
//            currentBrowser.js.exec(listChildren.get(1).firstElement(), "jQuery(arguments[0]).click();")
            FunctionalLibrary.Click(listChildren.get(1))
            Thread.sleep(20000)
        }else{
            throw new Exception("Given product is not available in ATP")
        }


    }


    def static void productAddToWishList(String zipCode, String quantity) {

        def listChildren = [];

        currentBrowser.at(ProductDetailsPage).buttonsContainer.children().each { x -> if (x.isDisplayed() == true) listChildren << x }

        String innerText = listChildren.get(0).text()

        if (!innerText.toLowerCase().contains("add to cart")) {

            FunctionalLibrary.Click(listChildren.get(0))

            FunctionalLibrary.SetText(currentBrowser.at(ProductDetailsPage).zipCodeText, zipCode)
            FunctionalLibrary.Click(currentBrowser.at(ProductDetailsPage).zipCodeSearchButton)
        }

        FunctionalLibrary.SetText(currentBrowser.at(ProductDetailsPage).productQuantity, quantity)
//        currentBrowser.js.exec(listChildren.get(1).firstElement(), "jQuery(arguments[0]).click();")
        FunctionalLibrary.Click(listChildren.get(1),30)

    }

//    def static void createAnotherWishListAtAddToWishListPopUp(String anotherWishListName) {
//
//        currentBrowser.at(WelcomePage).selectWishListsDropDown.click()
//        currentBrowser.at(WelcomePage).selectWishListsDropDown.find("option", text: "CREATE ANOTHER WISH LIST").click()
//        FunctionalLibrary.WaitForPageLoad(10)
////        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).selectWishListsDropDown.find("option", text: "CREATE ANOTHER WISH LIST"),5)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).newWishListNameField, anotherWishListName)
//        //FunctionalLibrary.Click(currentBrowser.at(WelcomePage).createWishListButton,5)
//        currentBrowser.js.exec(currentBrowser.at(WelcomePage).createWishListButton.firstElement(), "jQuery(arguments[0]).click();")
//        FunctionalLibrary.WaitForPageLoad(15)
//    }
//
//    def static void selectCreatedWishListOption() {
//        currentBrowser.at(WelcomePage).selectWishListsDropDown.click()
//        currentBrowser.at(WelcomePage).selectWishListsDropDown.children().first().next().click()
//        FunctionalLibrary.WaitForPageLoad()
////        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).selectWishListsDropDown.children().first().next(),5)
//    }

//    def static String getLastAddedWishList() {
//        def wishListColl = currentBrowser.at(WelcomePage).wishListingContainer.find("span", class: "whishListItemLinkBox")
//
//        return wishListColl.last().text()
//    }

//    def static void clickFirstAddedWishList() {
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).wishListingContainer.find("span", class: "whishListItemLinkBox").first())
//    }
//
//    def static void clickLastAddedWishList() {
//
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).wishListingContainer.find("span", class: "whishListItemLinkBox").last())
//    }


//    def static String lastaddedWishListItemCount() {
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).wishLists)
//        def lastAddedWishListItems = currentBrowser.at(WelcomePage).wishListingContainer.find("p").last()
//        return lastAddedWishListItems.text()
//    }


//    def static void wishListItemAddToCart() {
//
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).wishListContentAddToCart.find("a", text: "ADD TO CART"))
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).addToCartPopUpClose)
//    }
//
//
//    def static String createNewWishList(String wishListName) {
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).createNewWishListButton)
//        UUID uuid = UUID.randomUUID();
//        wishListName = wishListName + uuid.toString().substring(0, 6)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).newWishListNameField, wishListName)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).createWishListButton)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).createWishListSuccessPopUpClose)
//        return wishListName
//    }
//
//    def static void cancelNewWishList() {
////        Thread.sleep(2000)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).wishLists)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).createNewWishListButton)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).cancelWishListButton)
//    }
//
//    def static void chooseWishListActions(String actionName) {
//        def wishListActions = []
//        currentBrowser.at(WelcomePage).manageAccountContainer.find("a").each { x ->
//            if (x.getAttribute("class").startsWith("manageList")) {
//                wishListActions << x
//            }
//        }
//        for (int i = 0; i < wishListActions.size(); i++) {
//            if (wishListActions.getAt(i).text().toLowerCase().contains(actionName.toLowerCase())) {
//                FunctionalLibrary.Click(wishListActions.getAt(i))
//                break
//            }
//        }
//    }
//
//    def static void shareWishListDetails(String shareWishlistTo, String shareWishlistMessage) {
//
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).shareWishlistTo, shareWishlistTo)
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).shareWishlistMessage, shareWishlistMessage)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).shareWishListButton)
//    }
//
//    def static void editWishListDetails(String EditWishlistName) {
//
//        FunctionalLibrary.SetText(currentBrowser.at(WelcomePage).txtEditWishlistName, EditWishlistName)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).editSaveButton)
//
//    }

    def static double recalculatedTotalAtCart(int quantity) {
        double priceOfProduct = currentBrowser.at(CartPage).price.replace('$', "").replace(",","").toDouble()
        double total = priceOfProduct * quantity
        double Total=Math.round(total)
        return Total
    }

//    def static void createAccountAtThankyouPage(String password) {
//        FunctionalLibrary.SetText(currentBrowser.at(ThankYouPage).createPassword, password)
//        FunctionalLibrary.SetText(currentBrowser.at(ThankYouPage).confirmPassword, password)
//        FunctionalLibrary.Click(currentBrowser.at(ThankYouPage).createAccount)
//    }

    def static void moreColorSwatchAtSearchResultsPage() {
        def Products = currentBrowser.at(SearchResultsPage).products
        for (int i = 0; i < Products.size(); i++) {
            def ColorSwatch = Products.getAt(i).find("ul.product-swatches").find("li")
            def Size = ColorSwatch.size()
            //def Size = ColorSwatch.find("a[class ~= 'swatch-aspect-ratio']").size()
            if (Size > 5) {
               // def colorSwatchDisplay = ColorSwatch.find("a[class ~= 'product-swatches__swatch']").find("img").size()
                def colorSwatchMorelink = ColorSwatch.last().find("a.product-swatches__link").find("i")
                FunctionalLibrary.Click(colorSwatchMorelink,25)
               // return colorSwatchDisplay
                break

            }
        }

    }

    def static void updateZipCodeAtCartPage(zipCode){
        FunctionalLibrary.Click(currentBrowser.at(CartPage).updateZipCode, 10)
        FunctionalLibrary.SetText(currentBrowser.at(CartPage).zipCodeTextBox, zipCode)
        FunctionalLibrary.Click(currentBrowser.at(CartPage).updateZipCodeButton,20)
    }

//    def static void enterPromoCodeAtCartForAssert(String promocode) {
//        currentBrowser.page.interact { moveToElement(currentBrowser.at(CartPage).applyPromoCodeButton) }
//        currentBrowser.js.exec(currentBrowser.at(CartPage).applyPromoCodeButton.firstElement(), "jQuery(arguments[0]).click();")
////        FunctionalLibrary.SetText( currentBrowser.at(CartPage).enterPromoCodeLightBox,promocode)
//        currentBrowser.at(CartPage).enterPromoCodeLightBox.value(promocode)
//        FunctionalLibrary.Click(currentBrowser.at(CartPage).applyPromoCodeLightBox, 25)
//    }

    def static void productFlagStyleAtSearchResultsPage(String flagName,boolean IsQuickView = false) {
//        Boolean flag = false

        def Products =  currentBrowser.at(SearchResultsPage).products
        for(int i = 0; i<Products.size(); i++) {
            def ProductFlagStyle = currentBrowser.at(SearchResultsPage).products[i].find("div.product-grid-item__flag").first().find("span").text()
            if (ProductFlagStyle == flagName) {
                if (IsQuickView == false) {
//                    currentBrowser.js.exec(currentBrowser.at(SearchResultsPage).products[i].firstElement(), "jQuery(arguments[0]).mouseover();")
//                    currentBrowser.at(SearchResultsPage).products[i].click()
                    FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).products[i].find("img").first())
                    Thread.sleep(20000)
//                    FunctionalLibrary.WaitForPageLoad()
                } else {
//                    currentBrowser.js.exec(currentBrowser.at(SearchResultsPage).products[i].find("img").firstElement(), "jQuery(arguments[0]).mouseover();")
                    FunctionalLibrary.Focus(currentBrowser.at(SearchResultsPage).products[i].find("img").first())
                    Thread.sleep(10000)
                    currentBrowser.page.interact { moveToElement(currentBrowser.at(SearchResultsPage).products[i].find("img").first()) }
                    FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).products[i].find("a", text: "Quick View"))
                    Thread.sleep(20000)
//                    FunctionalLibrary.WaitForPageLoad()
                }
//                flag = true
                break
    }
}
//        if (flag == false) {
//            Products.get(Products.size() - 1) << Keys.PAGE_DOWN
//            productFlagStyleAtSearchResultsPage(flagName, true)
//        }

            }



//    def static void enterRsaIndicatorAtCartPage(String rsaNumber){
//        FunctionalLibrary.SetText(currentBrowser.at(CartPage).rsaIndicatorField, rsaNumber)
//        FunctionalLibrary.Click(currentBrowser.at(CartPage).rsaIndicatorApplyButton)
//    }


//To select price filter values at searchProductsPage in left side Checkbox




}