package Tests

import Pages.BillingPage
import Pages.CartPage
import Pages.ConfirmOrderPage
import Pages.Footer
import Pages.Header
import Pages.LoginPage
import Pages.OrderDetails
import Pages.PaymentPage
import Pages.PaypalPage
import Pages.ProductDetailsPage
import Pages.SearchResultsPage
import Pages.ShippingPage
import Pages.StoreLocatorPage
import Pages.ThankYouPage
import Pages.WelcomePage
import GroovyAutomation.CSVDataSource
import GroovyAutomation.FunctionalLibrary

import Utils.CommonFunctions
import geb.driver.CachingDriverFactory
import geb.spock.GebReportingSpec
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import spock.lang.Unroll

//@Stepwise
class SmokeTest extends GebReportingSpec {

    void resetBrowser() {
        def driver = browser.driver
        super.resetBrowser()
        driver.quit()
        CachingDriverFactory.clearCache()
    }

    def setup() {
        CommonFunctions.currentBrowser = getBrowser()
        FunctionalLibrary.currentBrowser = getBrowser()
    }

    @Unroll("01 - 112084: - SignIn Experience '#username' - 'Browser: #envBrowser' - '#envURL'")
    def "01 - 112084: - Sign In Experience - My Account Page"(){



        when: " Close overlay and Click Sign In option from My Account"

        //1. On the email sign up pop up, click on "No Thank You" link

        //2. Under My Account, click on "Sign In"
        CommonFunctions.myAccountOptionSelection("Sign In")

        "Login with existing user"

        //3. Enter a valid Email and Password and click on "Sign In" button
        at LoginPage
        userLogin(username, password)


        then:

        //4. Validate "My Account" title is displayed
        //5. Validate "Account Info" link is displayed
        //6. Validate "Order History" link is displayed
        //7. Validate "Address Book" link is displayed
        //8. Validate "Wish Lists" link is displayed

        at WelcomePage
        assert myAccountHeader.present
        assert accountInfo.present
        assert addressBook.present
        assert wishLists.present
        assert orderHistory.present

        where: "Getting Test Data from CSV"
        [username, password] << new CSVDataSource(System.getProperty("user.dir") + "\\src\\test\\groovy\\TestData\\112084.csv")
        envBrowser = System.getProperty("geb.env")
        envURL = System.getProperty("geb.build.baseUrl")

    }

}
