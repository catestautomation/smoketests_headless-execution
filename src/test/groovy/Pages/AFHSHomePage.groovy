package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary

public class AFHSHomePage extends Page{
    static url = ""

    static at =  {
        AshleyLogo.present
    }

    static content = {

        AshleyLogo{$("a#ctl09_afhsLogoLink")}

        //Links and Buttons
        noThankYouLink{$("a.successPopupReject")}
        noThankYouLinkClose{$("a#emailOverlayClose")}
//        myAccount { $("span", text: "My Account") }
        myAccount {$("img#ctl09_accountIcon")} // CTP2
//          myAccount {$("img#ctl10_accountIcon")} //QA2

    }

    def void closeOverlay() {
        FunctionalLibrary.Click(noThankYouLink)
//        FrameworkLibrary.WaitForPageLoad()
    }
}