package Pages

import geb.Page



class Header extends Page {

    static at = {
        body.present
    }

    static content = {

        AshleyLogo{$("a#ctl09_afhsLogoLink")}
        searchResultsAshleyLogo{$("div.logo").first().find("img")}
        body{$("body")}

        searchInputField {$("input#TbxSearch")}
        subCategorySearchBox{$("div.search-box")}
        SubCategorysearchInputField {subCategorySearchBox.$("input.search-box__input")}
        searchInputButton{$("#ctl09_searchIcon2")}
        SubCategorysearchInputButton{subCategorySearchBox.$("i.search-box__icon")}
        cartIcon{topMenu.$("img#ctl09_cartIcon")}

        //Container
        dropDownParent{$("ul#afhs-account-dropdown")}
        navigationParent { $("div[class ~= 'afhs-mega-nav']") }
        navigationParent1 { $("div[class ~= 'nav-bar']") }
        topMenu {$("div.afhs-top-menu")}
        findStore {topMenu.$("img#ctl09_locationIcon")}
        cartItemCount {topMenu.$("span").last().text()}

        searchSuggestionDDL{$("div.unbxd-as-wrapper.unbxd-as-extra-right")}
        productOnSearchSuggestionDDl{searchSuggestionDDL.$("li.unbxd-as-popular-product.unbxd-as-popular-product-grid").first()}

    }

}
