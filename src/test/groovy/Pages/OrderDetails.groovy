package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary


class OrderDetails extends Page {

    static at =  {

        heading.present

    }

    static content = {

        //Input Fields Elements
        heading { $ ("h2")}
        confirmationNumber {$("input#scphbody_0_UnregisteredOrder_TbxConfirmationNumber")}
        emailAddress {$("input#scphbody_0_UnregisteredOrder_TbxOrderEmail")}
        billingZipCode {$("input#scphbody_0_UnregisteredOrder_TbxBillingZip")}

        //Form
        orderDetailsForm{$("div#OrderDetailsForm")}

        //Button
        reviewOrder{$("a#scphbody_0_UnregisteredOrder_ViewOrder")}

        //Labels
        shippingAddressAtOrderDetailsPage{$("ul.inline-list").children().getAt(0)}
        billingAddressAtOrderDetailsPage{$("ul.inline-list").children().getAt(1)}
        paymentTypeAtOrderDetailsPage{$("ul.inline-list").children().getAt(2)}

        //Product Image
        productImageAtOrderDetailsPage {$("div.msax-ImageWrapper")}

        //Grid Content related elements
        productSummaryAtOrderDetailsPage{$("td.msax-Shipping")}
        productColorAtOrderDetailsPage{productSummaryAtOrderDetailsPage.$("div.msax-ShippingDetails.colorProduct")}
        productSizeAtOrderDetailsPage{productSummaryAtOrderDetailsPage.$("div.msax-ShipsInDetails").last().previous().find("span")}

        productPriceAtOrderDetailsPage{$("td.msax-Total")}

        productNameAtOrderDetailsPage{$("div.msax-ProductNameWrapper")}

        productQuantityAtOrderDetailsPage{$("td.msax-Quantity")}

        estHomeDeliveryAtOrderDetailsPage{$("tr.msax-EstHomeDelivery")}
        salesTaxAtOrderDetailsPage{$("tr.msax-Shipping")}
        totalAtOrderDetailsPage{$("tr.content-Total")}

    }

    def orderDetails(String orderNumber, String email, String zipCode) {
        FunctionalLibrary.SetText(confirmationNumber, orderNumber)
        FunctionalLibrary.SetText(emailAddress, email)
        FunctionalLibrary.SetText(billingZipCode, zipCode)
        FunctionalLibrary.Click(reviewOrder,150,true)
//        currentBrowser.at(OrderDetails).reviewOrder.click()
    }
}