package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary

class BillingPage extends Page{
    static at ={
        //assert billingHeader =="Billing" ; true
        useShippingAddressCheckbox.present
    }

    static content={

        billingHeader{$("h1").text()}
        useShippingAddressCheckbox{$("#UseShippingAddressCheckBox")}
        billingFirstName{$("#PaymentAddressFirstName")}
        billingLastName{$("#PaymentAddressLastName")}
        billingAddressLine1{$("#PaymentAddressStreet1")}
        billingCity{$("#PaymentAddressCity")}
        billingState{$("#PaymentAddressState")}
        billingZipCode{$("#PaymentAddressZipCode")}
        billingPhone{$("#PaymentPhoneNumber1")}
        billingEmail{$("#EmailTextBox")}
        billingConfirmEmail{$("#ConfirmBillingEmailTextBox")}
        continueButtonBilling{$("div.msax-BillingAddress").find(".msax-Next")}
        goBackButton { $("div.msax_BackButton").find("span") }

        topRedBarError {$("div.msax-ErrorPanel.msax-DisplayNone.msax-Error")}

        estimatedTax{$("tr.msax-Tax").find("span").getAt(1).text().replace('$',"").toDouble()}

        subTotal {$("tr.msax-SubTotal").find("span").getAt(1).text().replace('$', "").replace(",", "").toDouble()}
        homeDelivery{$("tr.msax-EstHomeDelivery").first().find("span").getAt(1).text().replace('$', "").toDouble()}

        estimatedRecyclingFee{$("tr.msax-EstHomeDelivery").last()}
        estimatedRecyclingFeeParent{estimatedRecyclingFee.$("td").first()}
        estimatedRecyclingFeeText{estimatedRecyclingFeeParent.$("span", text: "Est. State Recycling Fee")}
        estimatedRecyclingFeeChargeParent{estimatedRecyclingFee.$("td").last()}
        estimatedRecyclingFeeCharge{estimatedRecyclingFee.$("span.dollars")}

        recyclingFeePopUp{$("div#stateRecyclingFee")}
        recyclingFeePopUpCloseButton{$("a.lightboxCloseBtn.button.small.closeButton.right").last()}
        recyclingFeePopUpXClose{$("a.close-reveal-modal").last()}
    }

    def enterBillingDetails(boolean useShippingAddressCheckboxVerfication = true,String BillingFirstName="", String BillingLastName="", String BillingAddressLine1="", String BillingCity="", String BillingState="", String BillingZipCode="", String BillingPhone="", String BillingEmail="") {
        FunctionalLibrary.WaitForPageLoad()
        if (useShippingAddressCheckboxVerfication == true) {
            useShippingAddressCheckbox.value(true)
        } else {
            FunctionalLibrary.SetText(billingFirstName, BillingFirstName)
            FunctionalLibrary.SetText(billingLastName, BillingLastName)
            FunctionalLibrary.SetText(billingAddressLine1, BillingAddressLine1)
            FunctionalLibrary.SetText(billingCity, BillingCity)
            billingState.click()
            billingState.find("option", text: BillingState).click()
            FunctionalLibrary.SetText(billingZipCode, BillingZipCode)
            FunctionalLibrary.SetText(billingPhone, BillingPhone)
            FunctionalLibrary.SetText(billingEmail, BillingEmail)
            FunctionalLibrary.SetText(billingConfirmEmail, BillingEmail)

        }
        FunctionalLibrary.Click(continueButtonBilling)
        Thread.sleep(30000)
//        FunctionalLibrary.WaitForPageLoad()
    }
}

