package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary

class SearchResultsPage extends Page{

    static at = {
        heading.present
    }

    static content = {
        //Headers
        heading{$("h1")}
        AshleyLogo{$("img").findAll {x -> x.getAttribute("alt") == "Ashley HOMESTORE"}.first()}

        //Containers
        results {$("ul#resultsContainer_products") }
        searchProducts{results.$("li").findAll()}
//        searchresults{$("ul#resultsContainer_products") }
//        subCategoryResults{$("ul#resultsContainer_products")}
          subCategoryResults{$("ul.product-grid__list")}
//        subCategoryResults{$("ul.product-grid")}
        products {subCategoryResults.$("li").findAll()}
//        resultCountInfo {$ ("div.resultsCountInfo")}
        resultCountInfo {$ ("footer.product-grid__footer")}
        showingItemsInfo{resultCountInfo.$("div").first()}
        viewAllProductsButton {resultCountInfo.$("div.product-grid__actions").find("a")}

        searchInputField {$("input#TbxSearch")}
        searchInputButton{$("#ctl09_searchIcon2")}

        showingItemsParent {$("div.prodThumbResultCountTop")}
//        totalShowingItems{showingItemsParent.$("span.totalProductsCount").text()}
//        showingItemsParent {$("header.product-grid__header")}
        totalShowingItems{$("div.product-grid__showing-items").first().text()}
        facet{$("dl.accordion.subCategory").find("dd.first-item")}
        subCategoryfacet{$("div.collapse").find("div.facet-group.facet-group--active")}
        facetTitle{facet.$("div.contentTitleFacet").findAll()}
        facetContent {facet.$("div.content")}
        subCategoryfacetContent{subCategoryfacet.$("ul.facet-group__list")}

        priceCount {subCategoryfacetContent.first().$("em")}

        subCategorypriceCount {subCategoryfacetContent.first().$(".facet-item__count")}

        priceCheckBox{subCategoryfacetContent.first().$("li").first().find("input")}
        bedCount{facet.last().previous().previous().$("div.content").find("em")}
        totalProducts{$("dd#tab_products").find("em").text().replace("(","").replace(")","")}
        productCount{facetContent.$("em").text().findAll()}
        productTitle{results.$("div.product-title").find("a").find("h3").text()}
        narrowFilterCheckBox{facetContent("input.checkBoxSubCat")}


        productColorSwatchQuickView{quickViewLightBox.$("div#allColors").first()}
        colorNameQuickView{$("h5.swatchSingleValue").first().text()}
        colorSwatchOption{quickViewLightBox.$("div.aColor")}
        unselectedColor{colorSwatchOption.$("a").find("img")}
        selectedColorQuickView{quickViewLightBox.$("a.selectedColor").first().find("img").getAttribute("alt")}
        displayingProductSliderColorQuickView{$("div.sliderVideoPreviewContainer").first().find("img").getAttribute("alt")}
//        productsPrice{results.$("div.product-price-container")}
        productsPrice{results.$("div.product-price")}
        productCodeSearchResult{results.$("div.productImage")}
        suggestedSearch{$("label.did_you_mean").find("a")}

        productMoreOption{$("div.product-more-options")}

        //zipCode components for CheckPrice
        zipCodeSideBar {$("input#checkPriceZipCode")}
        localZipCode{$("span.product-zip-code__content-link", text: "Enter your zip code")}
//        zipcodeContainer{$("div#CheckPriceModal")}
//        zipCodeText  {zipcodeContainer.$("input#checkPriceModalInput")}
//        zipCodeSearchButton{zipcodeContainer.$("span.input-group-addon.button.postfix.fa.fa-search.zipCodeGlass").last()}
        zipcodeContainer{$("div.overlay__container")}
        zipCodeText  {zipcodeContainer.$("input.zip-code-overlay__input")}
        zipCodeSearchButton{zipcodeContainer.$("button.zip-code-overlay__button")}
        quickViewZipCodeContainer{$("div#quickViewCheckPriceModal")}
        quickViewZipCodeText{quickViewZipCodeContainer.$("input.form-control.numericOnly.zipCodeInput")}

        productLightBox{$("div#addToCart")}
        checkoutButtonAtLightBox {productLightBox.$("a", text: "CHECKOUT") }
        continueShoppingButton{productLightBox.$("#PopUpContinueShopping")}
        productImageAtLightBox{productLightBox.$("div.popupimg")}
        productNameAtLightBox{productLightBox.$("h5").find("em")}
        productPriceAtLightBox{productLightBox.$("h4.Price")}
        quantityAtLightBox{productLightBox.$("div.popupinfo",text: contains("Quantity:"))}
        colorAtLightBox{productLightBox.$("div.popupinfo",text: contains("Color:"))}
        estimatedShippingDateAtLightBox{productLightBox.$("div.popupinfo",text: contains("Usually delivers in"))}

        quickViewLightBox{$("div.overlay__container")}
        addToCartButtonQuickView{$("a#addToCartQuickView")}
        quickViewButtonsContainer {quickViewLightBox.$("div.product-quick-view-overlay__actions")}
        productImageatQuickViewLightbox{quickViewLightBox.$("div.product-slider")}
        productNameatQuickViewLightbox{quickViewLightBox.$("div.grid__col--md-4").find("h2")}
        productDescriptionatQuickViewLightbox {quickViewLightBox.$("p.product-quick-view-overlay__summary")}
        productPriceatQuickViewLightbox{quickViewLightBox.$("p.product-quick-view-overlay__price.mpRemove")}
        productColorsatQuickViewLightbox{quickViewLightBox.$("p.product-quick-view-overlay__color")}
        productQuantityatQuickViewLightbox{quickViewLightBox.$("div.product-quick-view-overlay__quantity")}
        quickViewLightBoxMouseOver{$("div.overlay__container")}
        quantityQuickViewLighBoxMouseOver{quickViewLightBoxMouseOver.$("div.product-quick-view-overlay__quantity")}
        productQuantityatQuickViewLightboxMouseOver{quantityQuickViewLighBoxMouseOver.$("input#quickViewQuantity")}
        viewProductDetailLinkatQuickViewlightbox{$("div.product-quick-view-overlay__url")}
        addToCartbtnatQuickViewLightbox{quickViewButtonsContainer.$("button", text: "Add To Cart")}
        addtoWishListatQuickViewLightbox{quickViewButtonsContainer.$("button", text: "Add To List")}
        socialMediaIconsatQuickViewLightbox{quickViewLightBox.$("div.social-share")}
        addToCartButtonMouseoverQuickView{quickViewLightBoxMouseOver.$("button.add-to-cart-button.button.button--primary")}
        checkoutButtonAtQuickViewLightBox {quickViewLightBox.$("button", text: "CHECKOUT") }
        continueShoppingButtonAtQuickView{quickViewLightBox.$("button", text: "CONTINUE SHOPPING")}
        productImageAtQuickViewCheckoutLightBox{quickViewLightBox.$("div.cart-response-overlay__product")}
        productNameAtQuickViewCheckoutLightBox{quickViewLightBox.$("header.cart-response-overlay__header").find("h3")}
        productPriceAQuickViewCheckOuttLightBox{quickViewLightBox.$("div.product-price")}
        quantityAtQuickViewCheckOutLightBox{quickViewLightBox.$("li",text: contains("Quantity:"))}
        colorAtQuickViewCheckOutLightBox{quickViewLightBox.$("li",text: contains("Color:"))}
        estimatedShippingDateAtQuickViewCheckOutLightBox{quickViewLightBox.$("li",text: contains("Usually delivers in"))}

        leftSidePriceFilterContainer{$("dl.accordion.subCategory.facets_container")}
        leftSidepriceFilter{leftSidePriceFilterContainer.find("ul#price-filter")}
        clearAll{$("a#clear_all_selected_facets")}
        sortByDropdown{$("select#productsSortBy")}
        sortByDrownByDefault{$("select#productsSortBy")}
        sortByDropDownSelected{sortByDrownByDefault.$("option").findAll().find{x -> x.getAttribute("selected value") != null}.text()}

        clearAllParent{$("div.width26.column").first()}
        clearAllFilters{clearAllParent.$("a",text: "Clear all")}
        subCategoryClearAllParent{$("header.product-facets__header")}
        subCategoryClearAllFilters{subCategoryClearAllParent.$("span", text: "Clear all")}
        arrowExpand{facet.$("a").find("img.arrowBlueSub.openArrow").findAll ()}
        arrowCollapse{facet.$("a").find("img.arrowBlueSub.closeArrow").findAll ()}
        subCategoryArrowExpand{subCategoryfacet.$("h3").findAll()}
        subCategoryArrowCollapse{$("div.collapse").find("div.facet-group").find("h3").findAll()}

    }

    static int index = 0;
    def selectProductFromSearchResultsPage(boolean IsSearchResults = "true") {

        //// FunctionalLibrary.WaitForPageLoad()
        //if (currentBrowser.at(SearchResultsPage).products.getAt(index).find('img').first().getAttribute("class") != "promo-card") {
        if (IsSearchResults == true) {
            if (results.children("li").getAt(index).find('img').first().getAttribute("class") != "promo-grid-item__image") {
                //  FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).products.getAt(index).find('img').first())
                FunctionalLibrary.Click(results.children("li").getAt(index).find('img').first())
                Thread.sleep(30000)
                FunctionalLibrary.WaitForPageLoad()
                index = 0;
            } else {
                index = index + 1
                selectProductFromSearchResultsPage(true)
            }
        } else {
            if (subCategoryResults.children("li").getAt(index).find('img').first().getAttribute("class") != "promo-grid-item__image") {
                //  FunctionalLibrary.Click(currentBrowser.at(SearchResultsPage).products.getAt(index).find('img').first())
                FunctionalLibrary.Click(subCategoryResults.children("li").getAt(index).find('img').first())
                Thread.sleep(30000)
                FunctionalLibrary.WaitForPageLoad()
                index = 0;
            } else {
                index = index + 1
                selectProductFromSearchResultsPage(false)
            }
        }
    }


    static def chosenProductIndex = null
    def clickCheckPriceOfProduct() {
        if (subCategoryResults.children("li").getAt(index).find('img').first().getAttribute("class") != "promo-grid-item__image") {
            chosenProductIndex = index
            FunctionalLibrary.Click(subCategoryResults.children("li").getAt(index).find("span", text: "Check price").first(),150,true)
            index = 0;
        } else {
            index = index + 1
            clickCheckPriceOfProduct()
        }
    }

    def clickCheckPriceFromSearchResults(String zipCode) {
        clickCheckPriceOfProduct()
        zipCodeText.value(zipCode)
        FunctionalLibrary.Click(zipCodeSearchButton)
        Thread.sleep(20000)
        FunctionalLibrary.WaitForPageLoad()
//        interact { moveToElement (subCategoryResults.children("li").getAt(chosenProductIndex).find('img').first()) }
//        def quickView = subCategoryResults.children("li").getAt(chosenProductIndex).find("a", text: "Quick View").findAll()
//        FunctionalLibrary.Click(quickView[0],20)
    }

    static int i = 0
    def clickQuickViewOfProduct(boolean IsSearchResultsPage = true) {
        ////FunctionalLibrary.WaitForPageLoad()
        if (IsSearchResultsPage == true) {
            def productContainer = results.children("li").getAt(i)
            if (productContainer.find('img').first().getAttribute("class") != "promo-grid-item__image") {
                FunctionalLibrary.Focus((productContainer.find('img').first()))
                //Thread.sleep(10000)
                interact { moveToElement(productContainer.find('img').first()) }
                Thread.sleep(10000)
                def quickView = productContainer.find("a", text: "Quick View").findAll()
//            currentBrowser.js.exec(quickView[0].firstElement(), "jQuery(arguments[0]).click();")
                FunctionalLibrary.Click(quickView[0],50)
//                FunctionalLibrary.WaitForPageLoad()
                i = 0;
            } else {
                i = i + 1
                clickQuickViewOfProduct()
            }
        } else {
            def productContainer = subCategoryResults.children("li").getAt(i)
            if (productContainer.find('img').first().getAttribute("class") != "promo-grid-item__image") {
                FunctionalLibrary.Focus((productContainer.find('img').first()))
                //Thread.sleep(10000)
                interact { moveToElement(productContainer.find('img').first()) }
                Thread.sleep(10000)
                def quickView = productContainer.find("a", text: "Quick View").findAll()
//            currentBrowser.js.exec(quickView[0].firstElement(), "jQuery(arguments[0]).click();")
                FunctionalLibrary.Click(quickView[0],30)
//                FunctionalLibrary.WaitForPageLoad()
                i = 0;
            } else {
                i = i + 1
                clickQuickViewOfProduct(false)
            }
        }
    }

    def sortBoxSelectField(String sortValue)
    {
        FunctionalLibrary.Click(sortByDropdown,150,true)
        FunctionalLibrary.Click(sortByDropdown.find("option",text: sortValue),150,true)
        Thread.sleep(20000)

    }

    def String checkLeftSortPriceFilter(){
        priceCheckBox.value(true)
        FunctionalLibrary.WaitForPageLoad()
//        FunctionalLibrary.Click(leftSidepriceFilter.find("li").find("h6").first())
//        String Price = priceCheckBox.next()
        String Price = subCategoryfacetContent.first().$("li").first().find("div.facet-item").find("label").text()
//        String Price = leftSidepriceFilter.find("li").find("h6").first().text()
        return Price
    }



}