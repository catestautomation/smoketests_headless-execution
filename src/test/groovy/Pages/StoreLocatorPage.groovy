package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary

class StoreLocatorPage extends Page {

    static at={
        storeLocatorHeader.present
    }

    static content ={
        //Text
        storeLocatorHeader { $("h1") }

        //Container
        findAStore { $("dd#FindStoreTab") }
        storeLocationList{$("dd#StoreLocationListTab")}
        locationInformation { $("div.locationContent") }
        distanceInfo { $("div.distantContent") }
        countryInfo { $("div.selectCountryContent") }
        results{ $("div#FindStore") }
        relatedSearchResults { $("div.main") }
        map { $("div#map_canvas") }
        storeDetailsParent{$("div.searchResults")}
        storeDetails{storeDetailsParent.$("li.standardStore").findAll()}
        parent{$("div#calendarStore")}
        shopDetails{$("div#descriptionStore")}
        addressAtShopDetail{$(parentOfShopDetail.find("address"))}
        mapAtShopDetailPage{$("div#contentGoogleMaps")}
        banner{$("div.banner")}
        openingHours{$("div.openingHours").first().find("li").findAll{}}
        storeHours{$("div#calendarStore").first().find("td").findAll()}

        resultDetails { results.$("div.resaultsOptions").find("h4").text() }

        findAStoreTab { findAStore.$("a", text: "FIND A STORE").getProperties().getAt("displayed") }

        //Button
        findAStoreButton { $("a.findStore", text: "FIND A STORE") }

        //Error Text
        errorText { $("span#StoreLocationError") }

        //Textbox, dropdown, links, Image
        location { $("input#TbxLocation") }
        distance { $("select#Tbxdistance") }
        country { $("select#Tbxcountry") }
        shopName {$("a#storeDetailsLink").first()}
        getDirection{$("p.getDirections").find("span",text:"Get directions").first()}
        stateLists {$("div.listCityforCountry").find("a")}
        homeStoreResults{$("div.resultsForcountry")}
        storeLocationCountry{$("select#slc")}
        distanceDropDown{$("option", text: Distance)}

        aboutTheStoreTitle{$("h2", text: "ABOUT THE STORE")}
        getDirectionAtShopDetails{parentOfShopDetail.$("a").find("span",text:"Get Directions")}
        getDirectionUnderMap{mapAtShopDetailPage.$("a").find("span",text:"Get Directions")}
        storeInfo{$("article.paragraph.leftImage")}
        storeImage{storeInfo.find("img")}
        storeText{storeInfo.find("p")}
        parentOfShopDetail{$("div#contentCalendarStore")}
        weeklyAdLink{$("ul.sub-nav").first().find("a")}
        moreNearbyStores{mapAtShopDetailPage.$("a", text: "More Nearby Stores")}

        //Mobile
        findAStoreButtonMob{$("a#mobileSearchButton")}
        storesNearMe{$("a#storesNearMe")}

    }

    def findAStore(String Location, String Distance, String Country) {
        FunctionalLibrary.SetText(location,Location)
        FunctionalLibrary.Click(distance,150,true)
        FunctionalLibrary.Click(distance.find("option", text: Distance),150,true)
        FunctionalLibrary.Click(country,150,true)
        FunctionalLibrary.Click(country.find("option", text: Country),150,true)
//        currentBrowser.at(StoreLocatorPage).distance.click()
//        currentBrowser.at(StoreLocatorPage).distance.find("option", text: Distance).click()
//        currentBrowser.at(StoreLocatorPage).country.click()
//        currentBrowser.at(StoreLocatorPage).country.find("option", text: Country).click()
        FunctionalLibrary.Click(findAStoreButton)
        FunctionalLibrary.WaitForPageLoad()

    }


}
