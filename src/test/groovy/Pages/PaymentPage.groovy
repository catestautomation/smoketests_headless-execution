package Pages
import GroovyAutomation.FunctionalLibrary

import geb.Page
class PaymentPage extends Page {
    static at = {
        heading.present
    }

    static content = {
        heading{$("h1")}
        enterPaymentInformation{$("h2").getAt(0)}

        activePaymentCardOptionContainer { $("ul.accordion").find("li.accordion-navigation.active.row") }
        activePaymentCard{ activePaymentCardOptionContainer.find("a").first()}

        //Credit Card value enter
        cardNumber{$("#CCard_CNber")}
        securityCode{$("#CCard_SecurityCode")}
        firstNameOnCard{$("#CCard_CardHolderFirstName")}
        lastNameOnCard{$("#CCard_CardHolderLastName")}
        expMonthOnCard{$("#CCard_ExpireMonth")}
        expYearOnCard{$("#CCard_ExpireYear")}
        continueButtonPayment{$("button#confirm-submission")}

        orderSummaryParent{$("div.msax-OrderSummary")}
        subTotal{orderSummaryParent.$("tr.msax-SubTotal")}
        estimatedHomeDelivery{orderSummaryParent.$("tr.msax-EstHomeDelivery")}
        estimatedTax{orderSummaryParent.$("tr.msax-Tax")}
        estimatedTotal {orderSummaryParent.$("tr.msax-Total")}

        paymentOptionCreditCard{$("#credit-card").getAt(0)}
        ashleyCardNumber{$("#AshleyAdvantageCard_AccountNumber")}
        ashleyAdvantageCard{$("a#advantage-card")}
        financingOptionContainer{$("div.radio-option")}
        viewFinanacingTerms{$("button#view-financing-terms")}
        termsOverlayAccept{$("button#accept-terms-button")}
        termsOverlayDecline{$("a", text: "Decline")}
        termsOverlayClose{$("a.close-reveal-modal").last()}
        takeAdvantage{$("a#take-advantage-action")}
        goBackButton { $("div.msax_BackButton").find("span") }

        payPal{$("a#pay-pal")}
        payPalcheckOutButton{$("div#payment-paypal-button")}

        salePromotion{$("tr.msax-DiscountTotal")}

        ashleyCardError{$("span", text: "Enter Ashley card number")}
        financingOptionError{$("span", text: "Please select a financing option below")}
        radioButtonOption{financingOptionContainer.$("input").findAll()}
        learnMorePopUp{$("div#learnmoreOverlay")}
        learnPopUpCloseButton{$("a.button.small.overlay-close").last()}

        estimatedRecyclingFee{$("tr.msax-StateRecyclingFee")}
        estimatedRecyclingFeeText{estimatedRecyclingFee.$("a", text: "State Recycling Fee:")}
        estimatedRecyclingFeeCharge{estimatedRecyclingFee.$("span").last()}

        recyclingFeePopUp{$("div#stateRecyclingOverlay")}
        recyclingFeePopUpCloseButton{$("a.button.small.overlay-close").last()}
        recyclingFeePopUpXClose{$("a.close-reveal-modal").last()}


    }

        def enterCreditCardDetails(String firstName, String LastName, String Card, String SecurityCode, String expMonth, String expYear) {
//        FunctionalLibrary.WaitForPageLoad()
            FunctionalLibrary.SetText(firstNameOnCard, firstName)
            FunctionalLibrary.SetText(lastNameOnCard, LastName)
            FunctionalLibrary.SetText(cardNumber, Card)
            FunctionalLibrary.SetText(securityCode, SecurityCode)
            expMonthOnCard.value(expMonth)
            expYearOnCard.value(expYear)
            FunctionalLibrary.Click(continueButtonPayment,25)
//        FunctionalLibrary.WaitForPageLoad()

        }

    def enterAshleyCardDetails(String ashleyCard, String financingOption) {
//        FunctionalLibrary.WaitForPageLoad()
        FunctionalLibrary.Click(ashleyAdvantageCard)
        FunctionalLibrary.SetText(ashleyCardNumber, ashleyCard)
        FunctionalLibrary.Click(financingOptionContainer.children().find { x -> x.text().contains(financingOption) })
        if(financingOption == '2020!' || '36 Months') {
            FunctionalLibrary.Click(takeAdvantage)
        }
        FunctionalLibrary.Click(viewFinanacingTerms,25)
//        FunctionalLibrary.WaitForPageLoad()
        FunctionalLibrary.Click(termsOverlayAccept,25)
//        FunctionalLibrary.WaitForPageLoad()
    }

    def payPalDetails() {
        FunctionalLibrary.Click(payPal)
        FunctionalLibrary.Focus(payPalcheckOutButton)
        FunctionalLibrary.Click(payPalcheckOutButton,150,true)
        FunctionalLibrary.WaitForPageLoad()
    }

}
