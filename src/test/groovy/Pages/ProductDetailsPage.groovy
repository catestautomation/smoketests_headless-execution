package Pages

import geb.Page
import geb.error.RequiredPageContentNotPresent
import geb.waiting.WaitTimeoutException
import org.openqa.selenium.Keys



class ProductDetailsPage extends Page{
    //static url = "/p/leo-full-panel-bed/apk-b103-fpp/"

    static at = {
//        productDiv.present
        header.present
    }

    static content = {


        header{$("head")}
        //Buttons
        addToCartButton{ $("#AddToCartProductDetail") }
        addToCartButtonQuickView{$("a#addToCartQuickView")}

        //Product details page elements
        productDiv { $(".afhs-content") }
        quickViewOverlay{$("div#js-loader-jWo8x9ZrkSzPODHD6fIg")}
        productQuantity {$(".productQuantity")}
        productQuantityMouseOver {$("input.product-quick-view-overlay__quantity-input")}
        productNamePDP {$("div.Product-SideInfo").find("h1").first()}
        productDetailImagesSlider{$("div#carousel-3")}
        socialMediaIcon{$("div.SocialIconsList.clearfix")}
        productSpecification {$("div[class ~= 'small-12']")}
        productDescription{$("div.medium-12.ProductDescription")}
        productPrice{$("h4.Price").first()}
        productPriceText{$("h4.Price").first().text().replace(" ","").replace('$','')}
        originalPrice{$("h4.originalPrice")}
        quickViewProductFlag{$("div.product-quick-view-overlay__flag").find("span")}
        productFlag{$("div#scphbody_0_prodFlagDiv")}

        fpoBannerWrapper{$("div.fpo-banner-wrapper").find("h2", text: "Financing")}

        //fpoBannerWrapper{$("div.column.medium-4.mpRemove").find("h3", text: "Financing")}
        fpoBanner{$("div.fpo-banner")}

        productColorSwatch{$("div#allColors").first()}
//        unSelectedColor{productColorSwatch.$("div.aColor").find("a[class ~= 'selectedColor']").last()}
        colorName{$("h5.swatchSingleValue").first().text()}
        colorSwatchOption{$("div.aColor")}
        unselectedColor{colorSwatchOption.$("a").find("img")}
        selectedColorBorder{$("a.selectedColor").first().find("img") }
        selectedColor{selectedColorBorder.getAttribute("alt")}
        displayingProductSliderColor{$("div.sliderVideoPreviewContainer").first().find("img").getAttribute("alt")}
        breadCrumbs {$("ul.breadcrumbs").find("li").last().previous().find("a")}

        learnMore{$("div.medium-4.column.LearnMore")}
        learnMoreOffersAndDetails {learnMore.$("ul").find("h4").children("a", text: "Offers & Details" )}
        learnMoreShopping {learnMore.$("ul").find("h4").children("a", text: "Shopping" )}
        learnMoreReturns {learnMore.$("ul").find("h4").children("a", text: "Returns" )}
        learnMoreWarrantyInfo {learnMore.$("ul").find("h4").children("a", text: "Warranty Information" )}
        learnMoreCare {learnMore.$("ul").find("h4").children("a", text: "Care & Cleaning" )}

        sizeForKitItems {$("select#DdlSize")}
        selectSize{sizeForKitItems.$("option")}

        //Container
        buttonsContainer { $("div.Shopbuttons") }
        buttonsContainerMouseOverQuickView{$("div.product-quick-view-overlay__actions")}

        //Textbox
        zipCodeText { $("#TbxAvailabilityZipCode") }
        zipCodeSearchButton{$("#AvailabilitySearchButton")}

        searchInputField {$("input#TbxSearch")}
        searchInputButton{$("#ctl09_searchIcon2")}

        //Lightbox elements
        productLightBox{$("div#addToCart")}
        checkoutButtonAtLightBox {productLightBox.$("a", text: "CHECKOUT") }
        continueShoppingButton{productLightBox.$("#PopUpContinueShopping")}
        productImageAtLightBox{productLightBox.$("div.popupimg")}
        productNameAtLightBox{productLightBox.$("h5").find("em")}
        productPriceAtLightBox{productLightBox.$("h4.Price")}
        quantityAtLightBox{productLightBox.$("div.popupinfo",text: contains("Quantity:"))}
        colorAtLightBox{productLightBox.$("div.popupinfo",text: contains("Color:"))}
        estimatedShippingDateAtLightBox{productLightBox.$("div.popupinfo",text: contains("Usually delivers in"))}
        notAvailabilityPopUp{$("div#notAvailability")}
        contactYourLocalStore{notAvailabilityPopUp.$("a", text: "Contact your local store")}
        updateDeliveryZipCode{notAvailabilityPopUp.$("a#updateDeliveryZip")}
        quickViewLightBoxClose{$("div.overlay__close-btn").last()}
        productFlagStyle{$("div.productFlagStyle")}

        productLightBoxMouseOver{$("div.overlay__container")}
        productNameAtLightBoxNouseOver{productLightBoxMouseOver.$("h3")}
        checkoutButtonAtLightBoxMouseOver {$("button", text: "CHECKOUT") }
        continueShoppingButtonMouseOver{$("button", text: "Continue Shopping")}

        quickViewLightBox{$("div.overlay__container")}
        productImageatQuickViewLightbox{quickViewLightBox.$("div.product-slider")}
        productNameatQuickViewLightbox{quickViewLightBox.$("div.grid__col--md-4").find("h2")}
        productDescriptionatQuickViewLightbox {quickViewLightBox.$("p.product-quick-view-overlay__summary")}
        productPriceatQuickViewLightbox{quickViewLightBox.$("p.product-quick-view-overlay__price")}
        productColorsatQuickViewLightbox{quickViewLightBox.$("p.product-quick-view-overlay__color")}
        productQuantityatQuickViewLightbox{quickViewLightBox.$("input.product-quick-view-overlay__quantity-input")}

        productImageAtQuickViewCheckoutLightBox{quickViewLightBox.$("div.cart-response-overlay__product")}
        productNameAtQuickViewCheckoutLightBox{quickViewLightBox.$("header.cart-response-overlay__header").find("h3")}
        productPriceAQuickViewCheckOuttLightBox{quickViewLightBox.$("div.product-price")}
        quantityAtQuickViewCheckOutLightBox{quickViewLightBox.$("li",text: contains("Quantity:"))}
        colorAtQuickViewCheckOutLightBox{quickViewLightBox.$("li",text: contains("Color:"))}
        estimatedShippingDateAtQuickViewCheckOutLightBox{quickViewLightBox.$("li",text: contains("Usually delivers in"))}
        checkoutButtonAtQuickViewLightBox {quickViewLightBox.$("button", text: "CHECKOUT") }
        continueShoppingButtonAtQuickView{quickViewLightBox.$("button", text: "CONTINUE SHOPPING")}
    }

}
