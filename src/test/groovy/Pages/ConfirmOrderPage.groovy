package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary

class ConfirmOrderPage extends Page {
    static at= {

        heading.present
    }

    static content={

        heading{$("h1").first()}
        orderConfirmContinueButton{$("button.msax-SubmitOrder.msax-PaddingLeft20").findAll(){it.displayed}}
        productRow { $("tr.msax-BorderTop1").findAll()}
        ashleyLogo{$("img.afhs-cart-logo")}
        goBackButton{$("div.msax_BackButton").find("span")}
        placeOrderButton{$("button", text: "Place Order")}
        navigateAwayLightBox{$("div#navigateAwayModal")}
        navigateAwayContinueButton{navigateAwayLightBox.$("a#navigateAwayContinue")}
        navigateAwayCancelButton{navigateAwayLightBox.$("a#navigateAwayCancel")}

        //Labels
        shippingAddress{$("div.msax-ShippingAddressData")}
        billingAddress{$("div.msax-BillingAddressData")}
        paymentType{$("div.msax-PaymentInfo").find("span",text:"Payment Type")}
        shippingMethod{$("div.msax-ShippingMethod")}
        orderSummary{$("div.msax-OrderSummary")}
        productName{$("a.msax-ProductName")}
        quantity{$("ul.msax-QuantityContent")}
        total {$("div.msax-LineTotal")}
        subTotal {$("tr.msax-SubTotal").find("span").getAt(1).text().replace('$', "").replace(",", "").toDouble()}

        paymentSection{$("div.msax-PaymentInfo").find("section").first().find("span").findAll()}

        editAddress{$("div#scphbody_0_OrderDetailEdit")}
        editShippingAddress{editAddress.$("div.msax-ShippingAddressData").find("span", text: "(Edit)")}
        editBillingAddress{editAddress.$("div.msax-BillingAddressData").find("span", text: "(Edit)")}
        editPaymentType{editAddress.$("div.msax-PaymentInfo").find("span", text: "(Edit)")}

        cartTable{$("table.msax-ItemsGrid")}
        cartSize{cartTable.$("div.msax-ProductQuickLink").findAll()}
        moveToWishList{cartSize.getAt(1).$("button.msax-Delete16.msax-WithText",text:"Move to Wish List")}

        labelProduct { $ ("label#LblProductAdded")}
        wishListMessage {labelProduct.children().first()}
        continueShoppingButton { $ ("a#WishListContinueShopping")}

        termsOfSubmitOrderButton {$("div#scphbody_0_termsSubmitWrapper")}
        termsOfUse{termsOfSubmitOrderButton.$("a", text: contains("Terms of Use"))}
        termsAndConditions{termsOfSubmitOrderButton.$("a", text: contains("Terms And Conditions"))}
        privacyPolicy{termsOfSubmitOrderButton.$("a", text: contains("Privacy Policy"))}

        termsOfUseLightBox{$("div#termsOfUseLightbox")}
        termsOfUseCloseButton {termsOfUseLightBox.$("a", text: contains("Close"))}
        termsAndConditionsLightBox{"div#termsAndConditionsLightbox"}
        termsAndConditionsCloseButton{termsAndConditionsLightBox.$("a", text: contains("Close"))}
        privacyPolicyLightbox{$("div#privacyPolicyLightbox")}
        privacyPolicyCloseButton{privacyPolicyLightbox.$("a", text: contains("Close"))}

        productRowRecyclingFee{productRow.get(0).$("a#StateRecyclingFee")}
        productRowRecyclingFeeText{productRowRecyclingFee.$("span", text: "State Recycling Fee:")}
        productRowRecyclingFeeCharge{productRow.get(0).$("span.dollars").first().text().replace('$', "").toDouble()}

        recyclingFeePopUp{$("div#stateRecyclingFee")}
        recyclingFeePopUpCloseButton{recyclingFeePopUp.$("a.lightboxCloseBtn.button.small.closeButton.right")}
        recyclingFeePopUpXClose{recyclingFeePopUp.$("a.close-reveal-modal")}

        estimatedRecyclingFee{$("tr.msax-EstHomeDelivery").last()}
        estimatedRecyclingFeeParent{estimatedRecyclingFee.$("td").first()}
        estimatedRecyclingFeeText{estimatedRecyclingFeeParent.$("span", text: "State Recycling Fee")}
        estimatedRecyclingFeeChargeParent{estimatedRecyclingFee.$("td").last()}
        estimatedRecyclingFeeCharge{estimatedRecyclingFee.$("span.dollars")}

        rsaIndicatorBlock{$("div#rsaIndicatorBlock")}
        rsaIndicatorField{rsaIndicatorBlock.$("input")}
//        rsaIndicatorApplyButton{rsaIndicatorBlock.$("button", text: "Apply")}// CT2
        rsaIndicatorApplyButton{rsaIndicatorBlock.$("button", text: "Apply Name")} //STG

        editRsaIndicatorBlock{$("div#showRsaIndicatorBlock")}
        rsaIndicatorText{editRsaIndicatorBlock.$("span.rsaIndicatorText")}
        editRsaIndicator{editRsaIndicatorBlock.$("button",text: "(edit)")}
        removeRsaIndicator{editRsaIndicatorBlock.$("button",text: "(remove)")}

        errorMessageAtConfirmOrderPage{$("div.msax-ErrorPanel.msax-DisplayNone msax-Info").find("span").text()}

        secureContent{$("div.secure-tile-content")}
        secureCheckOut{secureContent.$("h6", text: "Secure Checkout")}
        shoppingIsSafe{secureContent.$("p", text: "Shopping is always safe and secure")}

        leavePopUp{$("div#navigateAwayModal")}
        popUpMessage1{leavePopUp.$("p", text: "Leaving the checkout process will require you to re-enter your payment information when you return.").first()}
        popUpMesage2{leavePopUp.$("p", text: "Are you sure you want to do this?").last()}
        PopUpButton{$("div.row.text-right.buttonLightBox")}
        popUpCancel{PopUpButton.$("a#navigateAwayCancel")}
        popUpContinue{PopUpButton.$("a#navigateAwayContinue")}
        popUpClose{$("a.close-reveal-modal").last()}
        promoCode{$("div.msax-PromotionCodeStatus").find("span", text: "Click to Enter Promo Code(s)")}
        promocodePopUp{$("div#toPopup1")}
        enterPromoCodeLightBox { $("input#applyPromotionCodeID") }
        applyPromoCodeLightBox{$("button", text: "Apply").last()}
        closeLightBox { $("div.close") }
        applyPromoCodeButton { applyPromoCode.$("button.msax-Plus16") }
        applyPromoCode { $("div.msax-PromotionCodeStatus")}

        editPromoCodeLink {$("div.msax-PromotionCodeStatus").find("span")}

        orderSummaryRecyclingFee{$("a#StateRecyclingFee").find("span").last()}

    }

    def enterPromoCodeAtCart(String promoCode) {
        enterPromoCodeLightBox.value(promoCode)
        FunctionalLibrary.Click(applyPromoCodeLightBox)
        FunctionalLibrary.Click(closeLightBox)
    }
}


