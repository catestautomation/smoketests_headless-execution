package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary

class ThankYouPage extends Page {

    static at={
        orderConfirmationHeader.displayed
    }
    static content={
        //headers
        orderConfirmationHeader{$(".order-confirmation-page")}

        //labels
        thankYouPageContent{$("div.desciption.thank-you-text-wrapper")}
        orderNumber{$(".msax-Row").find("strong").first()}
        productRow { $("tr.msax-BorderTop1").findAll()}
        orderConfirmCloseOverlay {$("div.brdialog-close")}

        dialogWindowClose{$("div.brdialog-close",text:contains("Close"))}

        createPassword{$("input#scphbody_0_CreateGuestCustomer_UserCreatePassword")}
        confirmPassword{$("input#scphbody_0_CreateGuestCustomer_UserConfirmPassword")}
        createAccount{$("a#scphbody_0_CreateGuestCustomer_UserSubmitButton")}

        addressViewAtThankYouPage{$("div.msax-AddressView")}
        shippingAddressAtThankYouPage{addressViewAtThankYouPage.$("div.msax-ShippingAddressData")}
        billingAddressAtThankYouPage{addressViewAtThankYouPage.$("div.msax-BillingAddressData")}
        paymentInfoAtThankYouPage{addressViewAtThankYouPage.$("div.msax-PaymentInfo")}
        shippingMethodAtThankYouPage{paymentInfoAtThankYouPage.$("div.msax-ShippingMethod")}

        paymentSection{paymentInfoAtThankYouPage.find("section").first().find("span").findAll()}

        secureContent{$("div.secure-tile-content")}
        secureCheckOut{secureContent.$("h6", text: "Secure Checkout")}
        shoppingIsSafe{secureContent.$("p", text: "Shopping is always safe and secure")}

        productRowRecyclingFee{productRow.get(0).$("a#StateRecyclingFee")}
        productRowRecyclingFeeText{productRowRecyclingFee.$("span", text: "State Recycling Fee:")}
        productRowRecyclingFeeCharge{productRow.get(0).$("span.dollars").first().text().replace('$', "").toDouble()}

        recyclingFeePopUp{$("div#stateRecyclingFee")}
        recyclingFeePopUpCloseButton{recyclingFeePopUp.$("a.lightboxCloseBtn.button.small.closeButton.right").last()}
        recyclingFeePopUpXClose{recyclingFeePopUp.$("a.close-reveal-modal").last()}

        estimatedRecyclingFee{$("tr.msax-EstHomeDelivery").last()}
        estimatedRecyclingFeeText{estimatedRecyclingFee.$("span", text: "State Recycling Fee")}
        estimatedRecyclingFeeCharge{estimatedRecyclingFee.$("span.dollars")}

    }

    def createAccountAtThankyouPage(String password) {
        FunctionalLibrary.SetText(createPassword, password)
        FunctionalLibrary.SetText(confirmPassword, password)
        FunctionalLibrary.Click(createAccount)
    }

}
