package Pages

import geb.Page
import GroovyAutomation.FunctionalLibrary

class ShippingPage extends Page {
    static at = {
        firstName.present
    }

    static content = {
        useThisAddressButton { $(".msax-UseAddressButton") }
        useThisAddressPopUp { $("div#ui-id-3").findAll() }
        buttonCollections { $("div.ui-dialog-buttonset") }
        useThisAddressOption { buttonCollections.$("span", text: "Use This Address") }
        goBackButton { $("div.msax_BackButton").find("span") }
        continueButton { $(".msax-Next").getAt(0) }
        chooseShippingAddress { $("div.msax-ShippingAddressBoxItem").first().find("button") }
        orderSummaryParent { $("div.msax-OrderSummary") }
        subTotal { orderSummaryParent.$("tr.msax-SubTotal") }
        estimatedHomeDelivery { orderSummaryParent.$("tr.msax-EstHomeDelivery").first() }
        estimatedTotal { orderSummaryParent.$("tr.msax-Total") }
        taxMessage { orderSummaryParent.$("tr").find("span", text: "(Sales tax will be applied during checkout)") }

        estimatedRecyclingFee{$("tr.msax-EstHomeDelivery").last()}
        estimatedRecyclingFeeText{estimatedRecyclingFee.$("span", text: "Est. State Recycling Fee")}
        estimatedRecyclingFeeCharge{estimatedRecyclingFee.$("span.dollars")}

        recyclingFeePopUp{$("div#stateRecyclingFee")}
        recyclingFeePopUpCloseButton{$("a.lightboxCloseBtn.button.small.closeButton.right").last()}
        recyclingFeePopUpXClose{$("a.close-reveal-modal").last()}

        //textboxes
        firstName { $("#OrderAddressFirstName") }
        lastName { $("#OrderAddressLastName") }
        addressLine1 { $("#OrderAddressStreet1") }
        city { $("#OrderAddressCity") }
        state { $("#OrderAddressState") }
        zipCode { $("#OrderAddressZipCode") }
        phone { $("#OrderPhoneNumber1") }
        email { $("#ShippingEmail") }
        confirmEmail { $("#ShippingConfirmEmail") }

        firstNameErrorMsg { $("#OrderAddressFirstNameError").getProperties().getAt("displayed") }
        lastNameErrorMsg { $("#OrderAddressLastNameError").getProperties().getAt("displayed") }
        addressLine1ErrorMsg { $("#OrderAddressStreet1Error").getProperties().getAt("displayed") }
        cityErrorMsg { $("#OrderAddressCityError").getProperties().getAt("displayed") }
        stateErrorMsg { $("#OrderAddressStateError").getProperties().getAt("displayed") }
        phoneErrorMsg { $("#OrderPhoneNumber1Error").getProperties().getAt("displayed") }
        emailErrorMsg { $("#ShippingEmailError").getProperties().getAt("displayed") }
        confirmEmailErrorMsg { $("#ShippingConfirmEmailError").getProperties().getAt("displayed") }

    }

        def enterShippingDetails(String Firstname, String Lastname, String Addressline1, String City, String State, String Zipcode, String Phone, String Email) {
            Thread.sleep(20000)
            FunctionalLibrary.WaitForPageLoad()
            FunctionalLibrary.SetText(firstName, Firstname)
            FunctionalLibrary.SetText(lastName, Lastname)
            FunctionalLibrary.SetText(addressLine1, Addressline1)
            FunctionalLibrary.SetText(city, City)
            state.click()
            state.find("option", text: State).click()
            state.click()
            FunctionalLibrary.SetText(zipCode, Zipcode)
            FunctionalLibrary.SetText(phone, Phone)
            FunctionalLibrary.SetText(email, Email)
            FunctionalLibrary.SetText(confirmEmail, Email)
//        currentBrowser.page.interact { moveToElement(currentBrowser.at(ShippingPage).continueButton) }
//        currentBrowser.js.exec(currentBrowser.at(ShippingPage).continueButton.firstElement(), "jQuery(arguments[0]).click();")
            FunctionalLibrary.Click(continueButton,20)

            if (!useThisAddressPopUp.isEmpty()) {
                FunctionalLibrary.Click(useThisAddressOption)
            }
//          return continueButton
        FunctionalLibrary.WaitForPageLoad(20)
        }

}

