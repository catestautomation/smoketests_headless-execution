package Pages


import geb.Page
import GroovyAutomation.FunctionalLibrary

class WelcomePage extends Page {

    static at = {
        body.present
    }

    static content = {
        myAccountHeader { $("h1") }
        body{$("body")}
        //Container
        container{$("ul.tabs.vertical.large-3.column.hide-for-mobile")}
        suggestedAddress{$("div.reveal-modal.tiny.open")}


        wishLists{container.$("a",text: "Wish Lists")}
        orderHistory{container.$("a",text:"Order History")}
        accountInfo{container.$("a",text:"Account Info")}
        addressBook{container.$("a",text:"Address Book")}

        accountInfoCollection{$("div#AccountInfoGeneral")}
        accountInfoName{accountInfoCollection.$("span#userFullName")}
        accountInfoEmail{accountInfoCollection.$("span#userEmail").find("a", text: "(Edit)")}
        accountInfoPassword{accountInfoCollection.$("span.password.value").find("a", text: "(Edit)")}
        accountInfoNameEdit{accountInfoCollection.$("a", text: "(Edit)").first()}
        accountInfoNewFirstName{$("input#TbxFirstName")}
        accountInfoNewLastName{$("input#TbxLastName")}
        accountInfoNewSave{$("a#BtnSaveName")}
        accountInfoNewEmail{$("input#TbxEmail")}
        accountInfoConfirmNewEmail{$("input#TbxConfirmEmail")}
        accountInfoEmailPassword{$("input#TbxPassword")}
        accountInfoCurrentPassword{$("input#TbxCurrentPassword")}
        accountInfoNewPassword{$("input#TbxNewPassword")}
        accountInfoConfirmNewPassword{$("input#TbxConfirmNewPassword")}
        accountInfoPasswordSave{$("a#SaveNewPassword")}
        accountInfoEmailSave{$("a#SaveEmail")}
        invalidEmailError{$("div.forgotPass")}

        accountInfoSettings{$("a", text: "Settings")}
        accountInfoCheckBox{$("input#AccountInfoReceivePromotions")}

        //Wish list details
        wishListingContainerSideTab{$("div#wishListListing").find("ul").first()}
        wishListLast{wishListingContainerSideTab.children().last()}
        wishListTitle{$("div.large-6.column.topMobile").find("h4")}

        //Order history
        recentOrdersCollection{$("div#orders")}
        recentOrdersTab{recentOrdersCollection.$("a",text:"Recent Orders")}
        confirmationOrder{$("div.confirmationOrder")}

//        wishListContainer{$("div#contentAllItemsWhislist")}

        //Button
        addAddress{$("a#addNewAddressButton")}
        save{$("div.msax-CheckoutButtons").find("span",text: "SAVE")}
        useThisAddress{suggestedAddress.$("a#chooseSuggestedAddress")}
        addBillingAddress{$("a.editSelect.editBillingAddressLink",text:"(Add Billing Address)")}
        editBillingAddress{$("a.editSelect.editBillingAddressLink",text:"(Edit)")}


        //Text, dropdown
        firstName{$("input#TbxEditAddressFirstName")}
        lastName{$("input#TbxEditAddressLastName")}
        address{$("input#TbxEditAddress")}
        city{$("input#TbxEditAddressCity")}
        state{$("select#DdlStates")}
        zipCode{$("input#TbxEditAddressZip")}
        phoneNumber{$("input#TbxEditShippingPrimaryPhone")}
        email{$("input#TbxEditShippingEmail")}
        confirmEmail{$("input#TbxEditShippingConfirmEmail")}
        billingPhoneNumber{$("input#TbxEditBillingPrimaryPhone")}
        billingEmail{$("input#TbxEditBillingEmail")}
        billingConfirmEmail{$("input#TbxEditBillingConfirmEmail")}

        //Lables
        editShippingAddress{$("div.large-12.column.editAddressTitle").find("h4").text()}
        enteredShippingAddress{$("div#rowShippingAddress")}

        recentOrders{$("div#RecentOrders").find("h3").text()}

        //Address Book
        adressBookNoContentText{$("div.dashboard.clearfix").find("span.name.value").last().text()}

        //Shipping Book
        shippingAddressErrorVerfication{$("span.myAccountError.inline-error").findAll()}


        wishListingContainer { $ ("div#contentAllItemsWhislist")}
        wishListItem {wishListingContainer.$("span", class: "whishListItemLinkBox")}
        wishListItemCount {wishListingContainer.find("p")}
        createNewWishListButton { $ ("span", text: "CREATE NEW WISH LIST")}
        manageAccountContainer { $ ("div.tabs-content.vertical")}
        wishListItemsCollection {manageAccountContainer.$ ("div.selected.tile.itemContent").first()}
        wishListContentAddToCart {wishListItemsCollection.$ ("div.contentRemoveAddtocart")}
        addToCartPopUp { $ ("div#addToCart")}
        addToCartPopUpClose { $ ("a.close-reveal-modal").last() }
        wishListdetailsItem {wishListItemsCollection.$ ("div.detailsItem")}
        lastAddedWishList{$(wishListingContainer.find("span", class: "whishListItemLinkBox")).last()}

        itemImg {wishListItemsCollection.$("img.tile-icon")}
        itemPrice {wishListItemsCollection.$("h4.Price")}
        itemQty{wishListdetailsItem.$  ("span.colorItem")}
        itemName {wishListdetailsItem.$("span.title-item") }
        itemId {wishListdetailsItem.children().last()}

        shareWishlistTo { $ ("input#shareWishlistTo")}
        shareWishlistMessage { ($ ("textarea#shareWishlistMessage"))}
        shareWishListButton { $ ("a#shareWishListButton")}
        txtEditWishlistName { $ ("input#TxtEditWishlistName")}
        editSaveButton { $ ("a", text: "SAVE")}

        addToWishListPopUp {$ ("div#addToWishList")}
        selectWishListsDropDown  {addToWishListPopUp.$("select#DdlWishLists")}
        productImageatWishListPopUp{$ ("div.popupimg")}
        ProductNameInfo { $ ("div.popupinfo")}
        productNameatWishListPopUp {ProductNameInfo.$ ("h5")}
        continueShoppingButton { $ ("a#WishListContinueShopping")}
        viewWishListButton { $ ("a#ViewWishListDetails")}
        createAnotherWishList { $ ("option", text: "CREATE ANOTHER WISH LIST")}
        newWishListNameField { $ ("input#newWishListName")}
        wishListButtons { $ ("div.forgotPassbuttons")}
        createWishListButton  {wishListButtons.$ ("a", text: "CREATE")}
        cancelWishListButton {wishListButtons.$ ("a", text: "CANCEL")}
        labelProduct { $ ("label#LblProductAdded")}
        wishListMessage {labelProduct.children().first()}
        createWishListSuccessPopUp { $ ("div#createWhishListSuccess")}
        createWishListSuccessPopUpClose {createWishListSuccessPopUp.children().last()}

        addToWishListPopUpMouseOver {$ ("div.overlay__container")}
        selectWishListsDropDownMouseOver {addToWishListPopUpMouseOver.$("select.wish-list-overlay__select")}
        productImageatWishListPopUpMouseOver{$ ("div.wish-list-overlay__image")}
        ProductNameInfoMouseOver {$("div.wish-list-overlay__info")}
        popUpbuttons{addToWishListPopUpMouseOver.$("div.wish-list-overlay__actions")}
        productNameatWishListPopUpMouseOver {ProductNameInfoMouseOver.$ ("h5")}
        continueShoppingButtonMouseOver{popUpbuttons.$("button", text: "CONTINUE SHOPPING")}
        viewWishListButtonMouseOver { popUpbuttons.$("button", text: "View Wishlist")}
        createAnotherWishListMouseOver {selectWishListsDropDownMouseOver.$("option", text: "Create Another Wish List")}
        newWishListNameFieldMouseOver{$("input#wish-list-name")}
        wishListButtonsMouseOver{$("div.create-wish-list-overlay__actions")}
        createWishListButtonMouseOver {wishListButtonsMouseOver.$("button").last()}
        cancelWishListButtonMouseOver {wishListButtonsMouseOver.$("button").first()}
        wishListMessageMouseOver{$("p.wish-list-overlay__message")}


        viewOrderDetails{$("a.button.secondary.grey.small.buttonViewOrderDetails",text: contains("VIEW ORDER")).first()}
        confirmationOrderNumber{$("div.confirmationOrder").find("h5")}
        confirmationNumber{confirmationOrderNumber.find("strong").first().text().replace("#","")}

        paymentType{$("li.listItemsOrder").last().find("h1", text: "Payment Type").find("span").text().findAll()}

        productRow { $("tr.msax-BorderTop1").findAll()}

        productRowRecyclingFee{productRow.get(0).$("a#StateRecyclingFee")}
        productRowRecyclingFeeText{productRowRecyclingFee.$("span", text: "State Recycling Fee:")}
        productRowRecyclingFeeCharge{productRow.get(0).$("span.dollars").first().text().replace('$', "").toDouble()}

        recyclingFeePopUp{$("div#stateRecyclingFee")}
        recyclingFeePopUpCloseButton{$("a.lightboxCloseBtn.button.small.closeButton.right").last()}
        recyclingFeePopUpXClose{$("a.close-reveal-modal").last()}

        estimatedRecyclingFee{$("tr.msax-EstHomeDelivery").last()}
        estimatedRecyclingFeeText{estimatedRecyclingFee.$("span", text: "Est. State Recycling Fee:")}
        estimatedRecyclingFeeCharge{estimatedRecyclingFee.$("span.dollars").text().replace('$', "").replace(".00", "").toDouble()}

        productRowRecyclingFeeOrderDetails{productRow.get(0).$("a#OrderDetailContent_StateRecyclingFee")}
        productRowRecyclingFeeTextOrderDetails{productRowRecyclingFeeOrderDetails.$("span", text: "State Recycling Fee: ")}
        productRowRecyclingFeeChargeOrderDetails{productRow.get(0).$("span.color.value").first().text().replace('$', "").toDouble()}

        estimatedRecyclingFeeOrderDetails{$("tr.msax-StateRecyclingFee").last()}
        estimatedRecyclingFeeTextOrderDetails{estimatedRecyclingFeeOrderDetails.$("span", text: "State Recycling Fee: ")}
        estimatedRecyclingFeeChargeOrderDetails{estimatedRecyclingFeeOrderDetails.$("span").last().text().replace('$', "").replace(".00", "").toDouble()}

        divMainContent{$("div.main-content")}
        manageWishList{divMainContent.$("a.manageListLink")}
        divPopUpManageWishList{$("div#manageWishList")}
        tableManageWishList{($("table.manageWhishTable"))}
        tableWIshListRow{(tableManageWishList.$("tr").size())}
        saveButtonWishList{($("a#manageListsSaveBtn"))}
    }

    def myAccountSideTabOptionSelection(myAccountOption) {
        FunctionalLibrary.Click(container.find("a", text: myAccountOption).findAll {
            it.displayed
        })
//        FunctionalLibrary.WaitForPageLoad()
    }

    def clickLastWishListAtSideTab() {
        if (wishListingContainerSideTab.children().size() > 1) {
            FunctionalLibrary.Click(wishListLast,20)
        } else {
            throw new Exception("No wishlists were found for logged in user")
        }
    }

    def enterShippingDetailsAtAddressBook(String FirstName, String LastName, String Address, String City, String State, String Zipcode, String PhoneNumber, String Email) {
        FunctionalLibrary.SetText(firstName, FirstName)
        FunctionalLibrary.SetText(lastName, LastName)
        FunctionalLibrary.SetText(address, Address)
        FunctionalLibrary.SetText(city, City)
        state.click()
        state.find("option", text: State).click()
        FunctionalLibrary.SetText(zipCode, Zipcode)
        FunctionalLibrary.SetText(phoneNumber, PhoneNumber)
        FunctionalLibrary.SetText(email, Email)
        FunctionalLibrary.SetText(confirmEmail, Email)
        FunctionalLibrary.Click(save)
        //   FunctionalLibrary.WaitForPageLoad()
       //FunctionalLibrary.Click(useThisAddress)

    }

    def enterBillingDetailsAtAddressBook(String FirstName, String LastName, String Address, String City, String State, String Zipcode, String PhoneNumber, String Email) {
        FunctionalLibrary.SetText(firstName, FirstName)
        FunctionalLibrary.SetText(lastName, LastName)
        FunctionalLibrary.SetText(address, Address)
        FunctionalLibrary.SetText(city, City)
        state.click()
        state.find("option", text: State).click()
        FunctionalLibrary.SetText(zipCode, Zipcode)
        FunctionalLibrary.SetText(billingPhoneNumber, PhoneNumber)
        FunctionalLibrary.SetText(billingEmail, Email)
        FunctionalLibrary.SetText(billingConfirmEmail, Email)
        FunctionalLibrary.Click(save)
        //  FunctionalLibrary.WaitForPageLoad()
        FunctionalLibrary.Click(useThisAddress)
    }

    def wishListItemAddToCart() {

        FunctionalLibrary.Click(wishListContentAddToCart.find("a", text: "ADD TO CART"))
        FunctionalLibrary.Click(addToCartPopUpClose)
    }


    def createNewWishList(String wishListName) {
        FunctionalLibrary.Click(createNewWishListButton)
        UUID uuid = UUID.randomUUID();
        wishListName = wishListName + uuid.toString().substring(0, 6)
        FunctionalLibrary.SetText(newWishListNameField, wishListName)
        FunctionalLibrary.Click(createWishListButton)
        FunctionalLibrary.Click(createWishListSuccessPopUpClose)
    }

    def cancelNewWishList() {
//        Thread.sleep(2000)
        FunctionalLibrary.Click(wishLists)
        FunctionalLibrary.Click(createNewWishListButton)
        FunctionalLibrary.Click(cancelWishListButton)
    }

    def createAnotherWishListAtAddToWishListPopUp(String anotherWishListName) {

        selectWishListsDropDownMouseOver.click()
        selectWishListsDropDownMouseOver.find("option", text: "Create Another Wish List").click()
        Thread.sleep(20000)
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).selectWishListsDropDown.find("option", text: "CREATE ANOTHER WISH LIST"),5)
//        FunctionalLibrary.SetText(newWishListNameFieldMouseOver, anotherWishListName)
        newWishListNameFieldMouseOver.value(anotherWishListName)
        //FunctionalLibrary.Click(currentBrowser.at(WelcomePage).createWishListButton,5)
        createWishListButtonMouseOver.click()
        Thread.sleep(20000)
    }

    def selectCreatedWishListOption() {
        selectWishListsDropDown.click()
        selectWishListsDropDown.children().first().next().click()
        FunctionalLibrary.WaitForPageLoad()
//        FunctionalLibrary.Click(currentBrowser.at(WelcomePage).selectWishListsDropDown.children().first().next(),5)
    }

    def chooseWishListActions(String actionName) {
        def wishListActions = []
        manageAccountContainer.find("a").each { x ->
            if (x.getAttribute("class").startsWith("manageList")) {
                wishListActions << x
            }
        }
        for (int i = 0; i < wishListActions.size(); i++) {
            if (wishListActions.getAt(i).text().toLowerCase().contains(actionName.toLowerCase())) {
                FunctionalLibrary.Click(wishListActions.getAt(i))
                break
            }
        }
    }

    def shareWishListDetails(String shareWishlistToField, String shareWishlistMessageField) {

        FunctionalLibrary.SetText(shareWishlistTo, shareWishlistToField)
        FunctionalLibrary.SetText(shareWishlistMessage, shareWishlistMessageField)
        FunctionalLibrary.Click(shareWishListButton)
    }

    def editWishListDetails(String EditWishlistName) {

        FunctionalLibrary.SetText(txtEditWishlistName, EditWishlistName)
        FunctionalLibrary.Click(editSaveButton)

    }

}
